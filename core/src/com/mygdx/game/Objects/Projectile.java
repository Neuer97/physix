package com.mygdx.game.Objects;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.mygdx.game.Handlers_And_Components.B2Body.B2Body_Component;
import com.mygdx.game.Handlers_And_Components.Collisions.BodyUserData;
import com.mygdx.game.Handlers_And_Components.Collisions.ICollisionCallback;
import com.mygdx.game.Handlers_And_Components.Dragable.Drag_Component;
import com.mygdx.game.Handlers_And_Components.Explodable.Explosion_Component;
import com.mygdx.game.Handlers_And_Components.GameObjects.GameObject;
import com.mygdx.game.Handlers_And_Components.GameObjects.GameObjectTypes;
import com.mygdx.game.States.ST_game;

public class Projectile extends GameObject implements ICollisionCallback {
    B2Body_Component b2Body_component;
    Explosion_Component explosion_component;

    float bulletWidth;
    float bulletHeight;

    float bulletMass; //kg
    float dragConstant;
    float angularDamping = 1.5f;
    float density = 1f;

    int particleCount = 30;


    public Projectile(float bulletWidth, float bulletHeight, float bulletMass, float dragConstant, Vector2 barrelEnd, Vector2 tilt, float shootingForce, Vector2 startLinearVel) {
        super(GameObjectTypes.PROJECTILE);

        this.bulletHeight = bulletHeight;
        this.bulletWidth = bulletWidth;
        this.bulletMass = bulletMass;
        this.dragConstant = dragConstant;

        BodyDef bDef = ST_game.b2Body_creator.createBodyDef(barrelEnd.x, barrelEnd.y, BodyDef.BodyType.DynamicBody); //TODO: MAKE BULLET
        FixtureDef fDef = ST_game.b2Body_creator.createFixtureDef(
                ST_game.b2Body_creator.createRectShape(bulletWidth, bulletHeight, 0, 0, tilt.angle()),
                density, false, 1);

        B2Body_Component b2Body_component = new B2Body_Component(this, bDef, fDef, new BodyUserData(this, this), bulletMass);
        b2Body_component.b2body.setBullet(true);

        this.addComponent(b2Body_component);
        this.b2Body_component = b2Body_component;

        //FIRE!
        tilt.setLength(1);
        tilt.x *= shootingForce;
        tilt.y *= shootingForce;

        b2Body_component.b2body.setLinearVelocity(startLinearVel);
        b2Body_component.b2body.applyLinearImpulse(tilt, b2Body_component.b2body.getWorldCenter(), true);

        this.addComponent(defineDragComponent());

        explosion_component = defineExplosionComponent();
        this.addComponent(explosion_component);
    }

    public Drag_Component defineDragComponent() {
        return new Drag_Component(this, b2Body_component, new Vector2(bulletWidth, 0f), dragConstant, bulletMass, angularDamping);
    }

    public Explosion_Component defineExplosionComponent(){
        Explosion_Component explosion_component = new Explosion_Component(this);
        explosion_component.spawnExplosionParticles(new Vector2(100,100),new Vector2(bulletWidth, bulletHeight), particleCount);
        return explosion_component;
    }


    //TODO: Belongs to AmmunitionClass
    @Override
    public void beginCollisionCallback(GameObject gameObject_A, GameObject gameObject_B) {
        if(explosion_component.getExploded() || explosion_component.getExploding()){
            return; //TODO: "Despawn in Pool"
        }
        if(gameObject_A != null && gameObject_A.getType() == GameObjectTypes.EXPLOSION_PARTICLE){
            return;
        }
        if(gameObject_B != null && gameObject_B.getType() == GameObjectTypes.EXPLOSION_PARTICLE){
            return;
        }

        Vector2 radius = new Vector2(0,0);
        if(bulletWidth > bulletHeight) {
            radius.x = bulletWidth;
        }
        else {
            radius.x = bulletHeight; //That's Correct!
        }
        this.explosion_component.setPositionOfExplosion_SAVE(radius, b2Body_component.b2body.getPosition());
        this.explosion_component.setExplode(true);
    }

    @Override
    public void endCollisionCallback(GameObject gameObject_A, GameObject gameObject_B) {

    }
}


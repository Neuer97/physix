package com.mygdx.game.Objects;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.mygdx.game.Handlers_And_Components.AbstractComponent;
import com.mygdx.game.Handlers_And_Components.B2Body.B2Body_Component;
import com.mygdx.game.Handlers_And_Components.Collisions.BodyUserData;
import com.mygdx.game.Handlers_And_Components.GameObjects.GameObject;
import com.mygdx.game.Handlers_And_Components.GameObjects.GameObjectTypes;
import com.mygdx.game.Handlers_And_Components.Jointeable.Joint_Component;
import com.mygdx.game.States.ST_game;

public class Suspension extends GameObject {
    GameObject wheel;
    GameObject suspension;

    Joint_Component objectAttachment;


    float wheelPosX;
    float wheelPosY;

    float wheelRadius = 1;
    float wheelMass = 5;
    float wheeldensity = 1;
    float wheelfriction = 1;


    float suspensionPosX;
    float suspensionPosY;

    float suspensionWidth = 0.4f;
    float suspensionHeight = 1;
    float suspensionAngle = 0;
    float suspensionMass = 5;
    float suspensiondensity = 1;
    float suspensionfriction = 1;

    float motorSpeed = 0;
    float maxMotorTorque = 0;
    float suspensionDampingRatio = 60f;
    float suspensionFrequencyHZ = 50;

    public Suspension(GameObjectTypes type, float posX, float posY) {
        super(type);

        this.wheelPosX = posX;
        this.wheelPosY = posY;

        wheel = new GameObject(GameObjectTypes.WHEEL);
        B2Body_Component wheelB2BodyComp = defineWheelBodyComp();
        this.addComponent(wheelB2BodyComp);

        Joint_Component wheelJointComp = defineWheelJointComp(wheelB2BodyComp);


        suspension = new GameObject(this.getType());
        B2Body_Component suspensionBodyComp = defineSuspensionBodyComp();
        wheel.addComponent(suspensionBodyComp);

        Joint_Component suspensionWheelJointComp = defineSuspensionWheelJointComp(suspensionBodyComp);



        wheelJointComp.createWheelJoint(suspensionWheelJointComp, false, false,
                motorSpeed, maxMotorTorque, suspensionDampingRatio, suspensionFrequencyHZ,
                wheelB2BodyComp.b2body, suspensionBodyComp.b2body, new Vector2(0,1));

        defineObjectAttachment(suspensionBodyComp);
    }

    public Joint_Component defineObjectAttachment(B2Body_Component suspension){
        this.objectAttachment = new Joint_Component(this,suspension.b2body,0, suspensionHeight);
        return objectAttachment;
    }



    public B2Body_Component defineWheelBodyComp(){
        BodyDef bDef = ST_game.b2Body_creator.createBodyDef(wheelPosX, wheelPosY, BodyDef.BodyType.DynamicBody);
        FixtureDef fixtureDef = ST_game.b2Body_creator.createFixtureDef(ST_game.b2Body_creator.createCircleShape(wheelRadius),wheeldensity,false, wheelfriction);



        return new B2Body_Component(this, bDef, fixtureDef, new BodyUserData(null, this), wheelMass);
    }

    public Joint_Component defineWheelJointComp(B2Body_Component wheel){
        Joint_Component joint_component = new Joint_Component(this, wheel.b2body, 0,0);
        return joint_component;
    }




    public B2Body_Component defineSuspensionBodyComp(){
        BodyDef bDef = ST_game.b2Body_creator.createBodyDef(wheelPosX, wheelRadius + wheelPosY, BodyDef.BodyType.DynamicBody);
        FixtureDef fixtureDef = ST_game.b2Body_creator.createFixtureDef(ST_game.b2Body_creator.createRectShape(suspensionWidth, suspensionHeight,
                suspensionPosX, suspensionPosY,suspensionAngle ),suspensiondensity,false, suspensionfriction);

        wheel = new GameObject(GameObjectTypes.WHEEL);

        return new B2Body_Component(this, bDef, fixtureDef, new BodyUserData(null, this), suspensionMass);
    }

    public Joint_Component defineSuspensionWheelJointComp(B2Body_Component suspension){
        Joint_Component joint_component = new Joint_Component(this, suspension.b2body, 0,-suspensionHeight);
        return joint_component;
    }
}

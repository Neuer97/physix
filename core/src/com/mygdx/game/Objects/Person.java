package com.mygdx.game.Objects;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.MassData;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.Shape;
import com.mygdx.game.Handlers_And_Components.Animateable.Animatable_Component;
import com.mygdx.game.Handlers_And_Components.Animateable.Animation;
import com.mygdx.game.Handlers_And_Components.B2Body.B2Body_Component;
import com.mygdx.game.Handlers_And_Components.Collisions.BodyUserData;
import com.mygdx.game.Handlers_And_Components.Collisions.FootSensor.FootSensorCallback;
import com.mygdx.game.Handlers_And_Components.Drawable.Drawable_Component;
import com.mygdx.game.Handlers_And_Components.Forcable.Force_Component;
import com.mygdx.game.Handlers_And_Components.GameObjects.GameObject;
import com.mygdx.game.Handlers_And_Components.GameObjects.GameObjectTypes;
import com.mygdx.game.Handlers_And_Components.IUpdatable;
import com.mygdx.game.Handlers_And_Components.InputHandler.InputScheme;
import com.mygdx.game.Handlers_And_Components.Jointeable.Joint_Component;
import com.mygdx.game.Handlers_And_Components.Movable.Jumping_Component;
import com.mygdx.game.Handlers_And_Components.Movable.Moveable_Component;
import com.mygdx.game.Handlers_And_Components.MountAndPickUps.PickUp_Handler;
import com.mygdx.game.Handlers_And_Components.Shootable.IHandleInput;
import com.mygdx.game.States.ST_game;
import com.mygdx.game.Utils.B2Body_Creator;

import static com.mygdx.game.Constants.JUMPING_THRESHOLD;

public class Person extends GameObject implements IUpdatable, IHandleInput {
    B2Body_Component b2Body_component;
    Drawable_Component drawable_component;
    Animatable_Component animatable_component;

    PickUp_Handler pickUp_handler; //Can PickUp/Use Stuff
    Joint_Component joint_component; //Joint on which Usable "Snap" on

    Inventory inventory;

    float height = 1.8f;
    float width = 1.2f;
    private float mass = 80f;


    private float jumpingForce = 13f;
    long jumpingCooldown = 500; //in ms

    float desiredVelX = 25f;
    private float moveForceFactor = 0.07f;
    private float midAirMovmentFactor = 0.12f; //Reduces force in midAir
    private float friction = 1f;

    public Person() {
        super(GameObjectTypes.PERSON);
    }

    public void definePerson( float posX, float posY, PickUp_Handler pickUp_handler) {
        //Textures
        final TextureAtlas textureAtlas = new TextureAtlas("characterSprites/solider.pack");
        //________________________________________________________________________________________

        //BODY
        BodyUserData bodyUserData = new BodyUserData(null, this);
        b2Body_component = defineBody(ST_game.b2Body_creator, posX, posY, bodyUserData);
        //_____________________________________________________________________________

        //FORCE_COMPONENT
        Force_Component force_component = new Force_Component(this, b2Body_component.b2body);
        //_________________________________________

        //MOVE-ABILITIES
        Jumping_Component jumping_component = defineJumpable_Comp(force_component);
        defineMoveable_Comp(force_component, jumping_component);
        //__________________________________________

        //DRAWABLE
        drawable_component = defineDrawable(b2Body_component.b2body, textureAtlas);
        //_________________________________________

        //FOOT-SENSOR
        defineFootSensor(ST_game.b2Body_creator, b2Body_component.b2body, jumping_component, this, drawable_component.getSprite().getHeight());
        //_____________________________________________________________________________

        //ANIMATABLE
        animatable_component = defineAnimatableComponent(textureAtlas, drawable_component);
        //_________________________________________

        //JOINT_COMPONENT
        joint_component = defineJointComp();
        //_________________________________________

        this.pickUp_handler = pickUp_handler;
        this.inventory = new Inventory(this);
        this.addComponent(inventory);
    }

    private Animatable_Component defineAnimatableComponent(TextureAtlas textureAtlas, Drawable_Component drawable_component) {
        Animation animation1 = new Animation("running", 0, 0.1f, com.badlogic.gdx.graphics.g2d.Animation.PlayMode.LOOP,
                ST_game.animationArray_creator.createAnimationArray_stringManipulation(textureAtlas, "solider_running", 0, 6));


        return new Animatable_Component(this, drawable_component, animation1) {
            @Override
            public boolean animatableSpecific_update() {
                if(this.getLenghtOfCurrAnimations() == 0)
                    return false;
                else
                    return true;
            }
        };
    }

    private B2Body_Component defineBody(B2Body_Creator b2Body_creator, float posX, float posY,
                                        BodyUserData bodyUserData) {
        Shape shape = b2Body_creator.createRectShape(width, height, 0, 0, 0);

        B2Body_Component body_component = new B2Body_Component(this,
                b2Body_creator.createBodyDef(posX, posY, BodyDef.BodyType.DynamicBody),
                b2Body_creator.createFixtureDef(shape, 1f, false, friction),bodyUserData, 40f);

        body_component.b2body.setFixedRotation(true);
        body_component.b2body.setMassData(createMassData());
        return body_component;
    }

    private MassData createMassData() {
        MassData massData = new MassData();

        massData.center.set(0, 0);
        massData.I = 0; //Inertia;
        massData.mass = mass;

        return massData;
    }

    private Fixture defineFootSensor(B2Body_Creator b2Body_creator, Body b2body, Jumping_Component jumping_component, GameObject gameObject, float bodyHeight) {
        FootSensorCallback sensorCallback = new FootSensorCallback(gameObject, jumping_component);

        PolygonShape shape = b2Body_creator.createRectShape((width - (width * 0.10f)), 0.2f, 0, -height, 0);
        FixtureDef fDef = b2Body_creator.createFixtureDef(shape, 0, true, 0);
        Fixture fixture = b2body.createFixture(fDef);

        fixture.setUserData(new BodyUserData(sensorCallback, this)); //Is called when Colliding in WorldContactListener

        return fixture;
    }

    private Drawable_Component defineDrawable(Body b2body, TextureAtlas textureAtlas) {
        Drawable_Component drawable_component =
                new Drawable_Component(this, b2body, textureAtlas,
                        width * 2, height * 2, 0, 0,"solider_standing"); //TODO: Why Multiply by 2?
        return drawable_component;
    }

    private Jumping_Component defineJumpable_Comp(Force_Component force_component) {
        Jumping_Component jumping_component = new Jumping_Component(this, force_component, jumpingForce, jumpingCooldown,  JUMPING_THRESHOLD);
        return jumping_component;
    }

    private Moveable_Component defineMoveable_Comp(Force_Component force_component, Jumping_Component jumping_component) {
        Moveable_Component moveable_component = new Moveable_Component(this, force_component, desiredVelX, moveForceFactor, jumping_component, midAirMovmentFactor);
        return moveable_component;
    }

    private Joint_Component defineJointComp(){
        return new Joint_Component(this, b2Body_component.b2body, 0,0);
    }


    public boolean isArmed(){
        return this.joint_component.isSnapedOn();
    }

    @Override
    public void update(float dt) {
        if(this.b2Body_component.b2body.getLinearVelocity().x > 0.5f){
            drawable_component.setFlipX(false);
            animatable_component.addCurrentAnimation("running");
        }
        else if(this.b2Body_component.b2body.getLinearVelocity().x < -0.5f){
            drawable_component.setFlipX(true);
            animatable_component.addCurrentAnimation("running");
        }
        else {
            animatable_component.removeCurrentAnimation("running");
        }

        pickUp_handler.update(dt); //TODO: Make usablesHandler part of AbstractComponent to not have to call this (Or other Method for Sensors.. think about LevelSensors!)

    }


    public void handleInput(InputScheme inputScheme, float dt){
        Jumping_Component jumping_component = (Jumping_Component) this.getComponentsOfType(Jumping_Component.class.getName()).get(0);
        Moveable_Component moveable_component = (Moveable_Component) this.getComponentsOfType(Moveable_Component.class.getName()).get(0);

        if (inputScheme.isJumping()) {
            jumping_component.Jump(inputScheme.getMoving().y);
        }
        if (inputScheme.isMoving()) {
            moveable_component.move(inputScheme.getMoving().x);
        }


        if (inputScheme.isAiming()) {
                if (this.isArmed()) {
                    float degrees = (inputScheme.getAiming().angle());

                    ((Joint_Component) (this.getComponentsOfType(Joint_Component.class.getName()).get(0))).rotateTo(degrees);

                    if (inputScheme.isShootingPrimary()) {
                        ((Gun) (((Joint_Component) (this.getComponentsOfType(Joint_Component.class.getName()).get(0)))
                                .getOtherJointComponent().getParent())).use();
                    }
                }

        } else {
            ((Joint_Component) (this.getComponentsOfType(Joint_Component.class.getName()).get(0))).setMotorEnable(false);
        }
    }

}

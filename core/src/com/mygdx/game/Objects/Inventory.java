package com.mygdx.game.Objects;

import com.mygdx.game.Handlers_And_Components.AbstractComponent;
import com.mygdx.game.Handlers_And_Components.GameObjects.GameObject;
import com.mygdx.game.Handlers_And_Components.MountAndPickUps.Equipable_Component;

import java.util.LinkedList;
import java.util.List;

public class Inventory extends AbstractComponent {
    List<Equipable_Component> items;


    public Inventory(GameObject gameObject){
        super(gameObject);

        items = new LinkedList<Equipable_Component>();
    }

    public boolean hasEquipped(){
        return items.size() > 0;
    }

    public void equip(int i){
        items.get(i).equip(this.getParent());
    }

    public void pickUp(Equipable_Component equipable_component){
        this.items.add(equipable_component);


        if(this.items.size() == 1){
            equip(0); //If the first one is added equip it!
        }
    }

    public void throwAway(){
        if(items.size() > 0){
            items.get(0).unequip(this.getParent());
            items.remove(0);
        }
    }
}

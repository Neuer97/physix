package com.mygdx.game.Objects;

import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Shape;
import com.mygdx.game.Handlers_And_Components.B2Body.B2Body_Component;
import com.mygdx.game.Handlers_And_Components.Collisions.BodyUserData;
import com.mygdx.game.Handlers_And_Components.GameObjects.GameObject;
import com.mygdx.game.Handlers_And_Components.GameObjects.GameObjectTypes;
import com.mygdx.game.Handlers_And_Components.InputHandler.InputScheme;
import com.mygdx.game.Handlers_And_Components.Jointeable.Joint_Component;
import com.mygdx.game.Handlers_And_Components.MountAndPickUps.Mountable_Component;
import com.mygdx.game.Handlers_And_Components.MountAndPickUps.Pickupable_Component;
import com.mygdx.game.Handlers_And_Components.Shootable.IHandleInput;
import com.mygdx.game.States.ST_game;

public class Plane extends GameObject implements IHandleInput {
    B2Body_Component b2Body_component;

    Suspension suspension;
    Joint_Component suspensionJoint_component; //Where the Suspension is attached to

    Mountable_Component mountable_Component;
    GameObject pilot;


    float posX;
    float posY;

    float width = 7;
    float height = 1.5f;
    float mass = 300;
    float density = 1;
    float friction = 1;



    public Plane(float posX, float posY) {
        super(GameObjectTypes.PLANE);

        this.posX = posX;
        this.posY = posY;

        defineB2BodyComponent();
        defineMountableComp();

        defineSuspension();
        defineSuspensionJointComp(); //define Weld Point where suspensionJoint is attached to

        //Attach the Landing Gear
        suspensionJoint_component.createWeldJoint(this.suspension.objectAttachment,true);
    }

    public void defineB2BodyComponent(){
        BodyDef bDef = ST_game.b2Body_creator.createBodyDef(posX,posY, BodyDef.BodyType.DynamicBody);
        Shape shape = ST_game.b2Body_creator.createRectShape(width,height, 0,0, 0);

        FixtureDef fDef = ST_game.b2Body_creator.createFixtureDef(shape, density, false, friction);

        B2Body_Component b2Body_component = new B2Body_Component(this, bDef, fDef, new BodyUserData(null,this),mass);
        this.addComponent(b2Body_component);
        this.b2Body_component = b2Body_component;
    }

    public void defineSuspensionJointComp(){
        this.suspensionJoint_component = new Joint_Component(this, b2Body_component.b2body, width * 0.8f, -height);
    }

    public void defineSuspension(){
        this.suspension = new Suspension(this.getType(), this.posX, this.posY);
    }

    public void defineMountableComp(){
        mountable_Component = new Mountable_Component(this, this.b2Body_component.b2body);
        this.addComponent(mountable_Component);
    }

    @Override
    public void handleInput(InputScheme inputScheme, float dt) {
        //TODO:
    }
}

package com.mygdx.game.Objects;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.mygdx.game.Handlers_And_Components.B2Body.B2Body_Component;
import com.mygdx.game.Handlers_And_Components.Collisions.BodyUserData;
import com.mygdx.game.Handlers_And_Components.Forcable.Force_Component;
import com.mygdx.game.Handlers_And_Components.GameObjects.GameObject;
import com.mygdx.game.Handlers_And_Components.GameObjects.GameObjectTypes;
import com.mygdx.game.States.ST_game;

public class RotorBlades extends GameObject {
    B2Body_Component b2Body_component;
    Force_Component force_component;

    //TODO: Joint_Component to Plane

    float posX;
    float posY;

    float width = 7;
    float height = 2;
    float mass = 300;
    float density = 1;
    float friction = 1;

    public RotorBlades() {
        super(GameObjectTypes.ENGINE);

        defineBodyComp();
        defineForceComponent(b2Body_component.b2body);
    }

    //TODO: De-/Accelerate

    public void defineBodyComp(){
        BodyDef bdef = ST_game.b2Body_creator.createBodyDef(posX,posY, BodyDef.BodyType.DynamicBody);
        FixtureDef fdef = ST_game.b2Body_creator.createFixtureDef(
                ST_game.b2Body_creator.createRectShape(width, height, 0,0, 0)
                ,density,false,friction);

        b2Body_component = new B2Body_Component(this, bdef, fdef,new BodyUserData(null,this), mass);
    }

    public void defineForceComponent(Body body){
        force_component = new Force_Component(this, body);
    }
}

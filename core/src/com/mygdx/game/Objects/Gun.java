package com.mygdx.game.Objects;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Shape;
import com.mygdx.game.Handlers_And_Components.B2Body.B2Body_Component;
import com.mygdx.game.Handlers_And_Components.Collisions.BodyUserData;
import com.mygdx.game.Handlers_And_Components.Drawable.Drawable_Component;
import com.mygdx.game.Handlers_And_Components.GameObjects.GameObject;
import com.mygdx.game.Handlers_And_Components.GameObjects.GameObjectTypes;
import com.mygdx.game.Handlers_And_Components.IUsable;
import com.mygdx.game.Handlers_And_Components.Jointeable.Joint_Component;
import com.mygdx.game.Handlers_And_Components.MountAndPickUps.Equipable_Component;
import com.mygdx.game.Handlers_And_Components.Shootable.Shootable_Component;
import com.mygdx.game.Handlers_And_Components.MountAndPickUps.Pickupable_Component;
import com.mygdx.game.States.ST_game;

//TODO: Implement Weight for Gun && Bullets -> till that bullet is spawn in Weapon for Recoil
public class Gun extends GameObject implements IUsable {
    Drawable_Component drawable_component;
    B2Body_Component b2Body_component;
    Joint_Component joint_component;
    Shootable_Component shootable_component;

    Equipable_Component equipable_component;

    float width;
    float height;

    float maxMotorSpeed = (float) (0.000005 * Math.toDegrees(360));
    float motorTourque = 10000; //Just enough to shimmy on Edges

    int clipSize = 8;
    float reloadTime = 1.5f; //in seconds
    float RPM = 100; //Rounds per Minute
    float shootingForce = 1;
    float bulletWidth = .45f;
    float bulletHeigth = 0.15f;

    float gunMass = 20; //kg

    public Gun(float posX, float posY, float width, float height){
        super(GameObjectTypes.GUN);
        TextureAtlas textureAtlas = new TextureAtlas("WeaponsSprites/weapons.pack");

        this.width = width;
        this.height = height;

        Shape shape = ST_game.b2Body_creator.createRectShape(width, height, 0,0, 0);


        defineBodyComponent(posX,posY,shape);
        defineDrawableComp(textureAtlas);
        defineEquipableComp();
        defineJointComponent();
        defineShootableComp();
    }

    public void defineBodyComponent(float posX, float posY,Shape shape) {
        b2Body_component = new B2Body_Component(this,
                ST_game.b2Body_creator.createBodyDef(posX, posY, BodyDef.BodyType.DynamicBody),
                ST_game.b2Body_creator.createFixtureDef(shape, 1.0f, false, 0.7f),
                new BodyUserData(null, this), gunMass);
    }

    public void defineDrawableComp(TextureAtlas textureAtlas){
        drawable_component =
                new Drawable_Component(this, b2Body_component.b2body, textureAtlas,
                        width * 2, height * 2, 0, 0, "rocket_launcher"); //TODO: Why Multiply by 2?
    }

    public void defineJointComponent(){
        this.joint_component = new Joint_Component(this, b2Body_component.b2body, -width, 0);
    }

    public void defineEquipableComp(){
        equipable_component = new Equipable_Component(this, b2Body_component.b2body, maxMotorSpeed, motorTourque);
        this.addComponent(equipable_component);
    }

    public void defineShootableComp(){
        this.shootable_component = new Shootable_Component(this, b2Body_component,
                clipSize, reloadTime, RPM, shootingForce, bulletWidth, bulletHeigth);
    }
    @Override
    public void use() {
        Vector2 pos = b2Body_component.b2body.getWorldCenter();
        Vector2 impulse = new Vector2(1, 0); //TODO: Define ENDBARREL Vector like in Joint_Component | Maybe not neccesary bc weapon gets at spawn Obj-Coords (0,0) a nice recoil

        float angle = joint_component.getAngleDeg();
        impulse.rotate(angle);

        pos.mulAdd(impulse, width * 1.8f); // * 0.1f -> Spawn in Weapon -> Recoil
        this.shootable_component.shoot(pos, impulse);
    }
}

package com.mygdx.game.Scenes;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.graphics.Texture;
import com.badlogic.gdx.graphics.g2d.BitmapFont;
import com.badlogic.gdx.scenes.scene2d.Stage;
import com.badlogic.gdx.scenes.scene2d.ui.Button;
import com.badlogic.gdx.scenes.scene2d.ui.ImageButton;
import com.badlogic.gdx.scenes.scene2d.ui.Label;
import com.badlogic.gdx.scenes.scene2d.ui.Skin;
import com.badlogic.gdx.scenes.scene2d.ui.Table;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;
import com.badlogic.gdx.utils.Disposable;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mygdx.game.Main;

public class Hud implements Disposable{
    Main mainClass;

    //Scene2D.ui Stage and its own Viewport for HUD
    public Stage stage;
    private Viewport viewport;


    //Touchpad Lower-Left
    private float touchpadLL_deadzoneRad = 0;
    public Touchpad touchpadLL;

    //Touchpad Lower-Right
    private float touchpadLR_deadzoneRad = 0;
    public Touchpad touchpadLR;

    //Use Button
    Button button;

    //Values
    private Integer FPS = 0;

    //Corresponding Labels
    private Label FPS_Label;


    public Hud(Main mainClass){
        this.mainClass = mainClass;

        //Labels
        viewport = new FitViewport(Main.constants.V_WIDTH, Main.constants.V_HEIGHT, new OrthographicCamera());
        stage = new Stage(viewport, mainClass.spriteBatch);

        Table table = new Table();//define a table used to organize our hud's labels
        table.top();//Top-Align table
        table.left();
        table.setFillParent(true);//make the table fill the entire stage

        FPS_Label = new Label(String.format("%03d", FPS), new Label.LabelStyle(new BitmapFont(), Color.WHITE));
        table.add(FPS_Label);

        table.row();
        //____________________________________________________________________________________________


        //Touchpad Lower-Left
        touchpadLL = createTouchpad(new Texture("Controls/background.png"), new Texture("Controls/knob.png"),
                touchpadLL_deadzoneRad, 0,0, 100, 100);
        table.add(touchpadLL).padTop(85).padLeft(10);
        //____________________________________________________________________________________________
        //Touchpad Lower-Right
        touchpadLR = createTouchpad(new Texture("Controls/background.png"), new Texture("Controls/knob.png"),
                touchpadLR_deadzoneRad, 0,0, 100, 100);
        table.add(touchpadLR).padTop(85).padLeft(180);
        //____________________________________________________________________________________________



        //add our table to the stage
        stage.addActor(table);
        Gdx.input.setInputProcessor(stage);

        table.setDebug(true);
    }

    public Touchpad createTouchpad(Texture background, Texture knob, float deadZoneRadius, float x, float y, float width, float height) {
        Skin touchpadSkin = new Skin();
        //Set background image
        touchpadSkin.add("touchBackground", background);
        //Set knob image
        touchpadSkin.add("touchKnob", knob);
        //Create TouchPad Style
        Touchpad.TouchpadStyle touchpadStyle = new Touchpad.TouchpadStyle();
        //Apply the Drawables to the TouchPad Style
        touchpadStyle.background = touchpadSkin.getDrawable("touchBackground");
        touchpadStyle.knob = touchpadSkin.getDrawable("touchKnob");
        //Create new TouchPad with the created style
        Touchpad touchpad = new Touchpad(deadZoneRadius, touchpadStyle);
        //setBounds(x,y,width,height)
        touchpad.setBounds(x, y, width, height);

        return touchpad;
    }

    public void render(float dt) {
        mainClass.spriteBatch.setProjectionMatrix(stage.getCamera().combined);
        stage.draw();
    }
    public void update(float dt){
        FPS = Math.round((1 / dt));
        FPS_Label.setText(String.format("%03d", FPS));
    }

    public void resize(int width, int height){
        viewport.update(width,height);
    }

    @Override
    public void dispose() { stage.dispose(); }

}

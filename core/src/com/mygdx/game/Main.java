package com.mygdx.game;

import com.badlogic.gdx.Game;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.mygdx.game.States.ST_game;

public class Main extends Game {
    public SpriteBatch spriteBatch;

    //Constants
    public static Constants constants = new Constants();



    @Override
    public void create() {
        spriteBatch = new SpriteBatch();
        setScreen(new ST_game(this));
    }

    @Override
    public void render() {
        super.render();
    }

    @Override
    public void dispose() {
        spriteBatch.dispose();
    }
}

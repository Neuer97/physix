package com.mygdx.game.States;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Screen;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.maps.tiled.TiledMap;
import com.badlogic.gdx.maps.tiled.TmxMapLoader;
import com.badlogic.gdx.maps.tiled.renderers.OrthogonalTiledMapRenderer;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Box2DDebugRenderer;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.viewport.FitViewport;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mygdx.game.Constants;
import com.mygdx.game.Handlers_And_Components.B2Body.B2Body_Component;
import com.mygdx.game.Handlers_And_Components.Collisions.WorldContactListener;
import com.mygdx.game.Handlers_And_Components.Drawable.Drawable_Handler;
import com.mygdx.game.Handlers_And_Components.GameObjects.GameObject;
import com.mygdx.game.Handlers_And_Components.IUpdatable;
import com.mygdx.game.Handlers_And_Components.InputHandler.InputHandler;
import com.mygdx.game.Handlers_And_Components.InputHandler.InputScheme;
import com.mygdx.game.Handlers_And_Components.InputHandler.MouseAndKeysInputHandler;
import com.mygdx.game.Handlers_And_Components.InputHandler.TouchInputHandler;
import com.mygdx.game.Handlers_And_Components.MountAndPickUps.Equipable_Component;
import com.mygdx.game.Handlers_And_Components.MountAndPickUps.Mountable_Component;
import com.mygdx.game.Handlers_And_Components.MountAndPickUps.PickUp_Handler;
import com.mygdx.game.Handlers_And_Components.MountAndPickUps.Pickupable_Component;
import com.mygdx.game.Handlers_And_Components.Shootable.IHandleInput;
import com.mygdx.game.Main;
import com.mygdx.game.Handlers_And_Components.GameObjects.GameObjects_Handler;
import com.mygdx.game.Objects.Gun;
import com.mygdx.game.Objects.Inventory;
import com.mygdx.game.Objects.Person;
import com.mygdx.game.Objects.Plane;
import com.mygdx.game.Scenes.Hud;
import com.mygdx.game.Utils.AnimationArray_Creator;
import com.mygdx.game.Utils.B2Body_Creator;
import com.mygdx.game.Utils.TmxMapConverter;

import java.util.List;

/**
 * Created by Z003PH1H on 20.03.2018.
 */
//TODO: GENERAL
//Implement Rube -> More Features for creating a world
//TRY - CATCH! eg. Drawable -> Display Default Texture
//Collision Filtering (LibGDX-Documentation)
//B2BODY -> Breakable


public class ST_game implements Screen {
    Main mainClass;

    //Statics
    float accumulator = 0; //TimeAccumulator for timed world.step call

    //Helpers
    public static B2Body_Creator b2Body_creator = new B2Body_Creator();
    public static AnimationArray_Creator animationArray_creator = new AnimationArray_Creator();

    //Handlers
    GameObjects_Handler gameObjects_handler;
    PickUp_Handler pickUp_handler; //Same as GameObjects_Handler -> provides list of "Using Things in near"
    Drawable_Handler drawable_handler;
    InputHandler inputHandler;

    //CAMERA
    OrthographicCamera cam;
    Viewport viewport;

    //HUD
    Hud hud;

    //Map
    TmxMapLoader mapLoader;
    TiledMap map;
    OrthogonalTiledMapRenderer tileMapRenderer;
    private float mapScalar = (1f / 8f);

    //Box2D
    World world;
    Box2DDebugRenderer box2DDebugRenderer;
    TmxMapConverter mapConverter;

    //ContactListener
    WorldContactListener worldContactListener;

    //Test
    GameObject player;
    Gun gun;

    public ST_game(Main mainClass) {
        this.mainClass = mainClass;

        //Camera
        cam = new OrthographicCamera();
        viewport = new FitViewport(Main.constants.V_WIDTH / Main.constants.PPM, Main.constants.V_HEIGHT / Main.constants.PPM, cam);

        //HUD
        hud = new Hud(mainClass);

        //Map
        mapLoader = new TmxMapLoader();
        map = mapLoader.load("maps/testMap.tmx");
        tileMapRenderer = new OrthogonalTiledMapRenderer(map, mapScalar);

        //Box2D World/Map
        world = new World(new Vector2(0, Constants.GRAVITATIONAL_FORCE), Constants.DO_SLEEP);
        b2Body_creator.setWorld(world);
        box2DDebugRenderer = new Box2DDebugRenderer();

        mapConverter = new TmxMapConverter(mapScalar);
        mapConverter.AddMapLayerToBox2D(map.getLayers().get("ground"), world);

        //World-Contact-Listener
        worldContactListener = new WorldContactListener();
        world.setContactListener(worldContactListener);


        //Handlers
        gameObjects_handler = new GameObjects_Handler(world, cam, viewport);
        pickUp_handler = new PickUp_Handler(world, cam.position, viewport);
        drawable_handler = new Drawable_Handler(mainClass.spriteBatch);

        switch(Gdx.app.getType()) {
            case Android:
                inputHandler = new TouchInputHandler(hud.touchpadLL, hud.touchpadLR);
                break;
            case Desktop:
                inputHandler = new MouseAndKeysInputHandler(cam);
                break;
            case iOS:
                inputHandler = new TouchInputHandler(hud.touchpadLL, hud.touchpadLR); //TODO: Verify
                break;
        }

        //PLAYER-OBJECT
        player = new Person();
        ((Person)player).definePerson(140, 10, pickUp_handler);

        //TEST-PLAYER-OBJECT_________________________________________________________________________

        //TEST-GUN-OBJECT
        Gun gun = new Gun(130, 10, 1.0f, 0.8f);

        //TEST-PLANE-OBJECT
        Plane plane = new Plane(160, 12);
    }

    @Override
    public void show() {

    }

    public void handleInput(float dt) {

        InputScheme inputScheme = inputHandler.getInputScheme(((B2Body_Component)player.getComponentsOfType(B2Body_Component.class.getName()).get(0)).b2body.getWorldCenter());

        if(player instanceof IHandleInput){
            ((IHandleInput)player).handleInput(inputScheme,dt);
        }

        if(inputScheme.getIsUsingOnce()){
            if(!(player instanceof Person)){
                //Has Mounted
                Mountable_Component mountable_component = (Mountable_Component)player.getComponentsOfType(Pickupable_Component.class.getName()).get(0);//TODO: NullPointer!

                player = mountable_component.getUser();
                mountable_component.dismount();
            }
            else if(pickUp_handler.isUsableNear()){
                Pickupable_Component nearestUsable = pickUp_handler.getNearestPickup();
                Inventory inventory = (Inventory)player.getComponentsOfType(Inventory.class.getName()).get(0);

                if(nearestUsable instanceof Mountable_Component){
                    ((Mountable_Component) nearestUsable).mount(player);

                    player = nearestUsable.getParent();
                }
                else if(nearestUsable instanceof Equipable_Component){
                    inventory.pickUp((Equipable_Component) nearestUsable);
                }
            }
            else {
                Inventory inventory = (Inventory)player.getComponentsOfType(Inventory.class.getName()).get(0);
                inventory.throwAway();
            }
        }
    }

    public void timedUpdate(float deltaTime) {
        // fixed time step
        // max frame time to avoid spiral of death (on slow devices)
        float frameTime = Math.min(deltaTime, 0.25f);
        accumulator += frameTime;
        while (accumulator >= Constants.TIME_STEP) {
            update(deltaTime);
            accumulator -= Constants.TIME_STEP;
        }
    }

    public void update(float delta) {
        B2Body_Component b2Body_component = (B2Body_Component) player.getComponentsOfType(B2Body_Component.class.getName()).get(0); //TODO: NullPointerException


        handleInput(delta);

        //World-Update
        world.step(Constants.TIME_STEP, 6, 2);

        //Hud-Update
        hud.update(delta);

        //Cam-Update
        cam.position.set(b2Body_component.b2body.getPosition().x, b2Body_component.b2body.getPosition().y, 0);
        cam.update();
        tileMapRenderer.setView(cam);


        //UPDATE
        List<IUpdatable> updatables = gameObjects_handler.getVisibleIUpdatables();
        for (IUpdatable updatable : updatables) {
            updatable.update(delta);
        }
    }

    @Override
    public void render(float delta) {
        //Clear Screen
        Gdx.gl.glClearColor(0, 0, 1, 1);
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT);

        //Render Map
        tileMapRenderer.render();

        //Render Box2D Outlines
        box2DDebugRenderer.render(world, cam.combined);

        //Drawables
        mainClass.spriteBatch.setProjectionMatrix(cam.combined);
        drawable_handler.draw(gameObjects_handler.getVisibleDrawables());

        //Render HUD
        hud.render(delta);

        //Update Logic
        timedUpdate(delta);
    }


    @Override
    public void resize(int width, int height) {
        viewport.update(width, height);
        hud.resize(width, height);
    }

    @Override
    public void pause() {

    }

    @Override
    public void resume() {

    }

    @Override
    public void hide() {

    }

    @Override
    public void dispose() {

    }
}

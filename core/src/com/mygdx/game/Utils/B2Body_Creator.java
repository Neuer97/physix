package com.mygdx.game.Utils;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.CircleShape;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.Joint;
import com.badlogic.gdx.physics.box2d.JointDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.physics.box2d.joints.DistanceJoint;
import com.badlogic.gdx.physics.box2d.joints.DistanceJointDef;
import com.badlogic.gdx.physics.box2d.joints.RevoluteJoint;
import com.badlogic.gdx.physics.box2d.joints.RevoluteJointDef;
import com.badlogic.gdx.physics.box2d.joints.WeldJoint;
import com.badlogic.gdx.physics.box2d.joints.WeldJointDef;
import com.badlogic.gdx.physics.box2d.joints.WheelJoint;
import com.badlogic.gdx.physics.box2d.joints.WheelJointDef;
import com.mygdx.game.Handlers_And_Components.B2Body.B2Body_Component;
import com.mygdx.game.Handlers_And_Components.GameObjects.GameObject;
import com.mygdx.game.Main;

import java.util.List;

public class B2Body_Creator {
    World world;
    Vector2 dumpsterLocation = new Vector2(-100,-100);

    public void setWorld(World world) {
        this.world = world;
    }

    public Body createBody(BodyDef bDef){
        return world.createBody(bDef);
    }

    public BodyDef createBodyDef(float posX, float posY, BodyDef.BodyType bodyType) {
        BodyDef bdef = new BodyDef();
        bdef.position.set(posX, posY);
        bdef.type = bodyType;
        return bdef;
    }

    public FixtureDef createFixtureDef(Shape shape, float density, boolean isSensor, float friction) {
        FixtureDef fDef = new FixtureDef();
        fDef.shape = shape;
        fDef.density = density;
        fDef.isSensor = isSensor;
        fDef.friction = friction;
        return fDef;
    }

    //TODO: Refactor Joint Creation!
    public RevoluteJointDef createRevoluteJointDef(Body bodyA, Body bodyB, boolean collideConnected) {
        RevoluteJointDef jointDef = new RevoluteJointDef();
        jointDef.bodyA = bodyA;
        jointDef.bodyB = bodyB;
        jointDef.collideConnected = collideConnected;

        return jointDef;
    }

    public Joint createRevoluteJoint(Body bodyA, Body bodyB, boolean collideConnected,
                                     float localAnchorA_X, float localAnchorA_Y,
                                     float localAnchorB_X, float localAnchorB_Y, boolean motorEnabled, float motorSpeed, float maxMotorTorque) {
        RevoluteJointDef jointDef = createRevoluteJointDef(bodyA, bodyB, collideConnected);

        jointDef.enableMotor = motorEnabled;
        jointDef.motorSpeed = motorSpeed;
        jointDef.maxMotorTorque = maxMotorTorque;

        jointDef.localAnchorA.set(localAnchorA_X, localAnchorA_Y);
        jointDef.localAnchorB.set(localAnchorB_X, localAnchorB_Y);

        return world.createJoint(jointDef);
    }

    public Joint createRevoluteJoint(Body bodyA, Body bodyB, boolean collideConnected) {

        RevoluteJointDef jointDef = createRevoluteJointDef(bodyA, bodyB, collideConnected);
        jointDef.initialize(bodyA, bodyB, bodyA.getWorldCenter());
        Joint joint = world.createJoint(jointDef);
        return joint;
    }

    public WheelJointDef createWheelJointDef(float suspensionDampingRatio,float suspensionFrequencyHZ,
                                             boolean motorEnabled, float motorSpeed, float maxMotorTorque,
                                             Body bodyA, Body bodyB,
                                             float localAnchorA_X, float localAnchorA_Y,
                                             float localAnchorB_X, float localAnchorB_Y,
                                             Vector2 localAxisA, boolean collideConnected){

        WheelJointDef wheelJointDef = new WheelJointDef();
        wheelJointDef.dampingRatio = suspensionDampingRatio;
        wheelJointDef.frequencyHz = suspensionFrequencyHZ;
        wheelJointDef.enableMotor = motorEnabled;
        wheelJointDef.motorSpeed = motorSpeed;
        wheelJointDef.maxMotorTorque = maxMotorTorque;
        //wheelJointDef.localAxisA.set(localAxisA);

        wheelJointDef.bodyA = bodyA;
        wheelJointDef.bodyB = bodyB;

        wheelJointDef.localAnchorA.set(localAnchorA_X, localAnchorA_Y);
        wheelJointDef.localAnchorB.set(localAnchorB_X,localAnchorB_Y);
        wheelJointDef.collideConnected = collideConnected;

        return wheelJointDef;
    }

    public Joint createWheelJoint(float suspensionDampingRatio,float suspensionFrequencyHZ,
                                  boolean motorEnabled, float motorSpeed, float maxMotorTorque,
                                  Body bodyA, Body bodyB,
                                  float localAnchorA_X, float localAnchorA_Y,
                                  float localAnchorB_X, float localAnchorB_Y,
                                  Vector2 localAxisA, boolean collideConnected){

        WheelJointDef wheelJointDef = createWheelJointDef(suspensionDampingRatio, suspensionFrequencyHZ,
                motorEnabled, motorSpeed, maxMotorTorque,
                bodyA,bodyB,
                localAnchorA_X,localAnchorA_Y,
                localAnchorB_X,localAnchorB_Y,
                localAxisA, collideConnected);

        return world.createJoint(wheelJointDef);
    }
    public WeldJointDef createWeldJointDef(Body bodyA, Body bodyB,
                                           float localAnchorA_X, float localAnchorA_Y,
                                           float localAnchorB_X, float localAnchorB_Y, boolean collideConnected){
        WeldJointDef weldJointDef = new WeldJointDef();
        weldJointDef.bodyA = bodyA;
        weldJointDef.bodyB = bodyB;
        weldJointDef.collideConnected = collideConnected;

        weldJointDef.localAnchorA.set(localAnchorA_X, localAnchorA_Y);
        weldJointDef.localAnchorB.set(localAnchorB_X,localAnchorB_Y);

        return weldJointDef;
    }

    public WeldJoint createWeldJoint(Body bodyA, Body bodyB,
                                     float localAnchorA_X, float localAnchorA_Y,
                                     float localAnchorB_X, float localAnchorB_Y, boolean collideConnected){
        return (WeldJoint)world.createJoint(createWeldJointDef(bodyA,bodyB,localAnchorA_X,localAnchorA_Y,localAnchorB_X,localAnchorB_Y, collideConnected));
    }

    public DistanceJointDef createDistanceJointDef(Body bodyA, Body bodyB, boolean collideConnected) {
        DistanceJointDef jointDef = new DistanceJointDef();
        jointDef.bodyA = bodyA;
        jointDef.bodyB = bodyB;
        jointDef.collideConnected = collideConnected;

        return jointDef;
    }

    public Joint createDistanceJoint(Body bodyA, Body bodyB, boolean collideConnected, float length,
                                     float localAnchorA_X, float localAnchorA_Y,
                                     float localAnchorB_X, float localAnchorB_Y) {
        DistanceJointDef jointDef = createDistanceJointDef(bodyA, bodyB, collideConnected);

        Joint joint = world.createJoint(jointDef);
        joint.getAnchorA().set(localAnchorA_X, localAnchorA_Y);
        joint.getAnchorB().set(localAnchorB_X, localAnchorB_Y);

        jointDef.length = length;

        return joint;
    }

    public CircleShape createCircleShape(float radius) {
        CircleShape shape = new CircleShape();
        shape.setRadius(radius);
        return shape;
    }

    public EdgeShape createEdgeShape(float width, float height, float posX, float posY) {
        EdgeShape edgeShape = new EdgeShape();
        return edgeShape; //TODO: EdgeShape

    }

    public PolygonShape createRectShape(float width, float height, float posX, float posY, float angle) {
        PolygonShape shape = new PolygonShape();

        shape.setAsBox(width, height, new Vector2(posX, posY), (float)Math.toRadians(angle));
        return shape;
    }

    public World getWorld() {
        return world;
    }

    public void destroyJoint(Joint joint){
        world.destroyJoint(joint);
    }

    public void dump(GameObject gameObject){
        List<GameObject> gameObjects = gameObject.getGameObjects();

        for (GameObject gameObj:gameObjects) {
            List<B2Body_Component> body_components = ((List<B2Body_Component>)(Object)gameObj.getComponentsOfType(B2Body_Component.class.getName()));

            for (B2Body_Component bodyComp:body_components) {
                bodyComp.b2body.setTransform(dumpsterLocation, 0);
            }
        }
        B2Body_Component bodyComp;
        if(null != (bodyComp = (B2Body_Component)gameObject.getComponentsOfType(B2Body_Component.class.getName()).get(0))){
            bodyComp.b2body.setTransform(dumpsterLocation,0);
        }
    }

    public void reSpawn(GameObject gameObject, float posX, float posY, float angle){
        B2Body_Component b2Body_component;
        if(null != (b2Body_component = (B2Body_Component)gameObject.getComponentsOfType(B2Body_Component.class.getName()).get(0))) {
            b2Body_component.b2body.setTransform(posX,posY, angle);
        }
    }
}

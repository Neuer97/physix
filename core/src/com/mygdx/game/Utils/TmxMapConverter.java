package com.mygdx.game.Utils;

import com.badlogic.gdx.maps.MapLayer;
import com.badlogic.gdx.maps.MapObject;
import com.badlogic.gdx.maps.objects.RectangleMapObject;
import com.badlogic.gdx.math.Rectangle;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.ChainShape;
import com.badlogic.gdx.physics.box2d.EdgeShape;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.PolygonShape;
import com.badlogic.gdx.physics.box2d.Shape;
import com.badlogic.gdx.physics.box2d.World;
import com.mygdx.game.States.ST_game;

import java.util.LinkedList;
import java.util.List;

public class TmxMapConverter {
    float mapScalar; //If 16 Pixel Map a 1 Meter Object -> 1/16


    public TmxMapConverter(float mapScalar){
        this.mapScalar = mapScalar;
    }

    public void AddMapLayerToBox2D(MapLayer layer, World world) {

        Rectangle rect;
        BodyDef bDef;
        Shape shape;
        FixtureDef fixtureDef;
        Body body = null;

        List<EdgeShape> edgeShapes = new LinkedList<EdgeShape>();

        for(MapObject mapObject : layer.getObjects().getByType(RectangleMapObject.class)) {
            rect = ((RectangleMapObject) mapObject).getRectangle();
            rect.height *= mapScalar;
            rect.width *= mapScalar;
            rect.x *= mapScalar;
            rect.y *= mapScalar;

            bDef = ST_game.b2Body_creator.createBodyDef(rect.getX()  + (rect.getWidth() / 2),
                    rect.getY() + (rect.getHeight() / 2), //TODO: *2 and put /2 to orthogonalMapRenderer
                    BodyDef.BodyType.StaticBody);

            body = world.createBody(bDef);

            shape = ST_game.b2Body_creator.createRectShape((rect.getWidth() / 2), (rect.getHeight() / 2), 0,0, 0);
            edgeShapes.add(ST_game.b2Body_creator.createEdgeShape((rect.getWidth() / 2), (rect.getHeight() / 2), 0,0));

            fixtureDef = ST_game.b2Body_creator.createFixtureDef(shape, 1, false, 0.7f);

            body.createFixture(fixtureDef);
        }
    }
    public ChainShape createChainShape(List<Vector2> vertices) {
        //TODO: CreateChainShapes for seemliness map
        return null;
    }
}

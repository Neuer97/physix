package com.mygdx.game.Utils;

import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;

public class AnimationArray_Creator {
    public AnimationArray_Creator(){

    }

    //Creates an Animation per texturePacker Index ("run", 0; "run", 1; "run", 2; ...)
    public Array<TextureRegion> createAnimationArray_directIndex(TextureAtlas textureAtlas, String regionName, int firstIndex, int length) {
        Array<TextureRegion> frames = new Array<TextureRegion>();

        for(int i = firstIndex; i < firstIndex + length; i++) {
            frames.add(textureAtlas.findRegion(regionName, i));
        }
        return frames;
    }

    //Creates an Animation with various Names as String ("run_0"; "run_1"; ...)
    public Array<TextureRegion> createAnimationArray_stringManipulation(TextureAtlas textureAtlas, String regionName, int firstIndex, int length) {
        Array<TextureRegion> frames = new Array<TextureRegion>();

        for(int i = firstIndex; i < firstIndex + length; i++) {
            String name = regionName + i;
            frames.add(textureAtlas.findRegion(name));
        }

        return frames;
    }
}

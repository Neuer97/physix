package com.mygdx.game;

public class Constants {

    public final int V_WIDTH = 400;
    public final int V_HEIGHT = 208;
    public static final float PPM = 7;//1.3f; //PixelPerMeters -> Necessary bc box2D calculates physic in Meters,Kg, ...

    //World
    public static final boolean DO_SLEEP = false; //Whether Bodys can sleep or not
    // TODO: -> True, but till now necessary bc then bodies get not detected from GameObjects_Handler.Sensor -> (Gun)-Fixtures eg. are on Ground and sleeping -> no ContactListener is called
    //TimeStep
    public static final float TIME_STEP = 1f/45f;
    //Gravitation
    public static final float GRAVITATIONAL_FORCE = -9.81f;
    //Jumping-Threshold
    public static final float JUMPING_THRESHOLD = 0.5f;


}

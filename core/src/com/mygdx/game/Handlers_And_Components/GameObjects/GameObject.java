package com.mygdx.game.Handlers_And_Components.GameObjects;

import com.badlogic.gdx.physics.box2d.World;
import com.mygdx.game.Handlers_And_Components.AbstractComponent;
import com.mygdx.game.Handlers_And_Components.Drawable.Drawable_Component;
import com.mygdx.game.Handlers_And_Components.IDisposable;
import com.mygdx.game.Handlers_And_Components.IUpdatable;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;



public class GameObject implements IDisposable {
    List<GameObject> gameObjects = new LinkedList<GameObject>();
    HashMap<String, LinkedList<AbstractComponent>> components;

    GameObjectTypes type;

    public GameObject(GameObjectTypes type){
        components = new HashMap<String, LinkedList<AbstractComponent>>();
        this.type = type;
    }

    public void addComponent(AbstractComponent toAdd){
        String className;
        if(toAdd.getClass().getSuperclass().getSimpleName().equals("AbstractComponent")) {
            className = toAdd.getClass().getName(); //AbstractComponent ->
        }
        else { //bc Animatable inherits another time
            className = toAdd.getClass().getSuperclass().getName();
        }

        if(components.containsKey(className)) {
            (components.get(className)).add(toAdd);
        }
        else {
            components.put(className, new LinkedList<AbstractComponent>());
            (components.get(className)).add(toAdd);
        }
    }

    public void removeComponent(AbstractComponent toRemove) {
        components.remove(toRemove);
    }

    public LinkedList<AbstractComponent> getComponentsOfType(String type) {
        return components.get(type);
    }

    public List<IUpdatable> getUpdatableComponents(){
        List<IUpdatable> toRet = new LinkedList<IUpdatable>();

        for (Map.Entry<String, LinkedList<AbstractComponent>> entry : components.entrySet()) {

            if(entry.getValue().size() > 0 && (entry.getValue().get(0) instanceof IUpdatable)) {
                //Potential Errors here: First one implements, the others not -> But should work bc elements in one "Value" of HashMap are same class
                toRet.addAll((List<IUpdatable>)((Object)entry.getValue()));
                if(this instanceof IUpdatable){
                    toRet.add((IUpdatable) this);
                }
            }
        }

        if(toRet.size() < 1){
            return null;
        }
        return toRet;
    }

    public List<Drawable_Component> getDrawableComponents(){
        //Don't Start a fight with compiler - this sh.. works!
        return (List<Drawable_Component>)((Object)getComponentsOfType(Drawable_Component.class.getName()));
    }

    public List<IDisposable> getDisposableComponents(){
        List<IDisposable> toRet = new LinkedList<IDisposable>();

        for (Map.Entry<String, LinkedList<AbstractComponent>> entry : components.entrySet()) {

            if(entry.getValue().size() > 0 && (entry.getValue().get(0) instanceof IDisposable)) {
                //Potential Errors here: First one implements, the others not -> But should work bc elements in one "Value" of HashMap are same class
                toRet.addAll((List<IDisposable>)((Object)entry.getValue()));
                if(this instanceof IDisposable){
                    toRet.add((IDisposable) this);
                }
            }
        }

        if(toRet.size() < 1){
            return null;
        }
        return toRet;
    }

    public void setType(GameObjectTypes type) {
        this.type = type;
    }

    public GameObjectTypes getType() {
        if(type == null){
            return GameObjectTypes.DEFAULT;
        }
        return type;
    }

    @Override
    public void dispose() {
        List<IDisposable> disposables = getDisposableComponents();

        for (IDisposable disposable:disposables) {
            if(disposable != this)
                disposable.dispose();
        }
    }

    public List<GameObject> getGameObjects() {
        return gameObjects;
    }
}

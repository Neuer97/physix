package com.mygdx.game.Handlers_And_Components.GameObjects;

import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mygdx.game.Handlers_And_Components.Collisions.BodyUserData;
import com.mygdx.game.Handlers_And_Components.Collisions.ICollisionCallback;
import com.mygdx.game.Handlers_And_Components.Drawable.Drawable_Component;
import com.mygdx.game.Handlers_And_Components.GameObjects.GameObject;
import com.mygdx.game.Handlers_And_Components.IUpdatable;
import com.mygdx.game.States.ST_game;

import java.util.AbstractMap;
import java.util.LinkedList;
import java.util.List;

public class GameObjects_Handler implements ICollisionCallback , IUpdatable {
    OrthographicCamera camera;
    private Viewport viewport;
    private float scalingFactor = 0.2f; //Initial Height && Width needs to be scaled that Textures are not popping in on the ScreenSides
    private Vector2 position = new Vector2();
    private Body sensorBody;

    List<GameObject> visibleGameObj = new LinkedList<GameObject>();
    List<IUpdatable> visibleIUpdatables = new LinkedList<IUpdatable>();
    List<Drawable_Component> visibleDrawables = new LinkedList<Drawable_Component>();

    public GameObjects_Handler(World world, OrthographicCamera camera, Viewport viewport){
        this.camera = camera;
        this.viewport = viewport;

        BodyDef bDef = ST_game.b2Body_creator.createBodyDef(camera.position.x, camera.position.y, BodyDef.BodyType.StaticBody);
        sensorBody = world.createBody(bDef);

        float width = viewport.getWorldWidth() * scalingFactor;
        float height = viewport.getWorldHeight() * scalingFactor;
        FixtureDef fdef = ST_game.b2Body_creator.createFixtureDef(
                (ST_game.b2Body_creator.createRectShape(width,height, 0,0, 0)),
                0, true, 0);
        fdef.filter.maskBits = 1; //No Collision //TODO: Right collision-Filtering

        sensorBody.createFixture(fdef).setUserData(new BodyUserData(this, null));
        visibleIUpdatables.add(this);
    }

    @Override
    public void update(float dt) {
        position.set(camera.position.x, camera.position.y);
        sensorBody.setTransform(position,0);
        //TODO: SetHeight && Width
    }

    @Override
    public void beginCollisionCallback(GameObject gameObject_A, GameObject gameObject_B) {
        GameObject gameObject = null;
        if(gameObject_A != null){
            gameObject = gameObject_A;
        }
        if(gameObject_B != null){
            gameObject = gameObject_B;
        }
        if(gameObject == null) {
            System.out.println("GameObjects_Handler: both GameObjects were null");
            return;
        }

        if(visibleGameObj.contains(gameObject)) {
           return;
        }
        visibleGameObj.add(gameObject);
        if(gameObject.getDrawableComponents() != null)
            visibleDrawables.addAll(gameObject.getDrawableComponents());

        if(gameObject.getUpdatableComponents() != null)
            visibleIUpdatables.addAll(gameObject.getUpdatableComponents());
    }

    @Override
    public void endCollisionCallback(GameObject gameObject_A, GameObject gameObject_B) {
        GameObject gameObject;
        if(gameObject_A != null){
            gameObject = gameObject_A;
        }
        else if(gameObject_B != null){
            gameObject = gameObject_B;
        }
        else {
            System.out.println("GameObjects_Handler: both GameObjects were null");
            return;
        }

        visibleGameObj.remove(gameObject); //TODO: Try Catch

        if(gameObject.getUpdatableComponents() != null)
            visibleIUpdatables.removeAll(gameObject.getUpdatableComponents());

        if(gameObject.getDrawableComponents() != null)
            visibleDrawables.removeAll(gameObject.getDrawableComponents());
    }

    public void addIUpdateable(IUpdatable iUpdatabletoAdd){
        this.visibleIUpdatables.add(iUpdatabletoAdd);
    }

    public List<Drawable_Component> getVisibleDrawables() {
        return visibleDrawables;
    }

    public List<GameObject> getVisibleGameObj() {
        return visibleGameObj;
    }

    public List<IUpdatable> getVisibleIUpdatables() {
        return visibleIUpdatables;
    }
}

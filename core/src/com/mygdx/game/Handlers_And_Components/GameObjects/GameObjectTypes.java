package com.mygdx.game.Handlers_And_Components.GameObjects;

public enum GameObjectTypes {
    PROJECTILE,
    GUN,
    PERSON,
    EXPLOSION_PARTICLE,
    GROUND, //TODO: FIND GROUND
    WHEEL,
    ENGINE,
    PLANE,
    DEFAULT,
}
package com.mygdx.game.Handlers_And_Components.Movable;

import com.mygdx.game.Handlers_And_Components.Forcable.Force_Component;
import com.mygdx.game.Handlers_And_Components.GameObjects.GameObject;
import com.mygdx.game.Handlers_And_Components.AbstractComponent;

public class Moveable_Component extends AbstractComponent {
    private float forceFactor;
    Force_Component force_component;
    Jumping_Component jumpingComponent;

    float desiredVelocityX;

    float midAirMovmentFactor;

    public Moveable_Component(GameObject parent, Force_Component force_component, float desiredVelocityX,
                              float forceFactor, Jumping_Component jumping_component, float midAirMovmentFactor) {
        super(parent);

        this.force_component = force_component;
        this.forceFactor = forceFactor;
        this.desiredVelocityX = desiredVelocityX;
        this.jumpingComponent = jumping_component;

        this.midAirMovmentFactor = midAirMovmentFactor;
    }

    public void move(float percentage) {
        float desiredVel = desiredVelocityX * percentage;
        float impulse = ((desiredVel - force_component.b2body.getLinearVelocity().x) * force_component.b2body.getMass()) * forceFactor;


        //Is on ground
        if(jumpingComponent.getJumpingEnabled()) {
            force_component.applyImpulseToCenter(impulse, 0f);
        }
        else {
            force_component.applyImpulseToCenter(impulse  * midAirMovmentFactor, 0f);
        }
    }
}

package com.mygdx.game.Handlers_And_Components.Movable;

import com.mygdx.game.Handlers_And_Components.Forcable.Force_Component;
import com.mygdx.game.Handlers_And_Components.GameObjects.GameObject;
import com.mygdx.game.Handlers_And_Components.AbstractComponent;

//TODO: Make Jumping && Moveable to X & Y Components, then Walking, Driving, Jumping, Flying (?)
public class Jumping_Component extends AbstractComponent {
    //Body b2Body; //Only one Ref, bc multiple Body-Parts are coded with multiple Fixtures!
    Force_Component force_component;

    boolean jumpingEnabled = true; //Shows if Body is on Ground or not
    float force;
    long lastJump = 0; //For deactivating Gravity
    long jumpingCooldown;

    float jumpingThreshold;

    public Jumping_Component(GameObject gameObject, Force_Component force_component,
                             float force, long jumpingCooldown, float jumpingThreshold) {
        super(gameObject);

        this.force_component = force_component;
        this.force = force;
        this.jumpingCooldown = jumpingCooldown;
        this.jumpingThreshold = jumpingThreshold;
    }

    public void Jump(float knobPercent) {
        if(knobPercent < jumpingThreshold){
            return;
        }
        if(jumpingEnabled && System.currentTimeMillis() > lastJump + jumpingCooldown ) {
            lastJump = System.currentTimeMillis();

            float impulse = force_component.b2body.getMass() * force;
            force_component.applyImpulseToCenter(0, impulse);
        }
    }

    public void setJumpingEnabled(boolean jumpingEnabled){
        this.jumpingEnabled = jumpingEnabled;
    }
    public boolean getJumpingEnabled(){
        return jumpingEnabled;
    }
}

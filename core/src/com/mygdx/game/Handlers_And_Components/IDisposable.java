package com.mygdx.game.Handlers_And_Components;

public interface IDisposable {
    void dispose();
}

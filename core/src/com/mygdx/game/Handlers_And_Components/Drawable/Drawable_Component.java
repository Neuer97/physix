package com.mygdx.game.Handlers_And_Components.Drawable;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.TextureAtlas;
import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.physics.box2d.Body;
import com.mygdx.game.Handlers_And_Components.GameObjects.GameObject;
import com.mygdx.game.Handlers_And_Components.AbstractComponent;


public class Drawable_Component extends AbstractComponent {
    private Body b2Body; //For Referencing the World-Coordinates
    private float offsetX; //For Referencing in Object-CoordinatesX
    private float offsetY; //For Referencing in Object-CoordinatesY

    private TextureAtlas textureAtlas; //all or some(of the same type) textures

    private TextureRegion defaultTexture;
    
    private Sprite sprite;
    private boolean flipX;
    private boolean flipY;

    public Drawable_Component(GameObject gameObject, Body b2Body_ref,TextureAtlas textureAtlas, float width, float height, float offsetX, float offsetY, String defaultRegion) {
        super(gameObject);
        this.b2Body = b2Body_ref;
        this.textureAtlas = textureAtlas;

        this.offsetX = offsetX;
        this.offsetY = offsetY;

        sprite = new Sprite();
        defaultTexture = textureAtlas.findRegion(defaultRegion);
        sprite.setRegion(textureAtlas.findRegion(defaultRegion));

        sprite.setBounds(0,0, width,height);
    }

    public void setRegion(String region){
        sprite.setRegion(textureAtlas.findRegion(region));
    }

    public Sprite getSprite(){
        return sprite;
    }

    public Body getB2Body() {
        return b2Body;
    }

    public float getOffsetX() {
        return offsetX;
    }

    public float getOffsetY() {
        return offsetY;
    }

    public void setAnimatedTexture(TextureRegion animatedTexture) {
        this.sprite.setRegion(animatedTexture);
    }

    public void resetAnimatedTexture(){
        this.sprite.setRegion(defaultTexture);
    }

    public void setFlipX(boolean flipX){
        this.flipX = flipX;
        sprite.setFlip(flipX, flipY);
    }

    public void setFlipY(boolean flipY){
        this.flipY = flipY;
        sprite.setFlip(flipX, flipY);
    }
}

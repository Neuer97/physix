package com.mygdx.game.Handlers_And_Components.Drawable;

import com.badlogic.gdx.graphics.g2d.Sprite;
import com.badlogic.gdx.graphics.g2d.SpriteBatch;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.Fixture;

import java.util.List;

public class Drawable_Handler {
    SpriteBatch spriteBatch;


    public Drawable_Handler(SpriteBatch spriteBatch) {
        this.spriteBatch =spriteBatch;
    }

    public void draw(List<Drawable_Component> toDraw){
        for (Drawable_Component drawable : toDraw) {
            draw(drawable.getSprite(), drawable.getB2Body(), drawable.getOffsetX(), drawable.getOffsetY());
        }
    }

    private void draw(Sprite sprite, Body b2Body, float offsetX, float offsetY) {
        sprite.setPosition((b2Body.getPosition().x - (sprite.getWidth() / 2)) + offsetX, (b2Body.getPosition().y - (sprite.getHeight() / 2)) + offsetY);

        sprite.setOrigin((sprite.getWidth() / 2) - offsetX, (sprite.getHeight() / 2) - offsetY);
        sprite.setRotation((float) (b2Body.getAngle() * (180 / Math.PI)));

        //TODO: Reduce drawCalls by groupingTextures!
        spriteBatch.begin();
        sprite.draw(spriteBatch);
        spriteBatch.end();
    }
}

package com.mygdx.game.Handlers_And_Components.Explodable;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.mygdx.game.Handlers_And_Components.AbstractComponent;
import com.mygdx.game.Handlers_And_Components.B2Body.B2Body_Component;
import com.mygdx.game.Handlers_And_Components.Collisions.BodyUserData;
import com.mygdx.game.Handlers_And_Components.GameObjects.GameObject;
import com.mygdx.game.Handlers_And_Components.GameObjects.GameObjectTypes;
import com.mygdx.game.Handlers_And_Components.IUpdatable;
import com.mygdx.game.States.ST_game;

import java.util.LinkedList;
import java.util.List;

public class Explosion_Component extends AbstractComponent implements IUpdatable {
    List<GameObject> particles = new LinkedList<GameObject>();

    short groupIndex = -1; //not colliding with each other

    float particlesMass = 10;
    float explosionForce = 500;

    float linearDamping = 7; //Drag bc moving Air
    float gravityScale = 0; //No Gravity
    float restitution = 0.99f; //High Bounciness
    float density = 420f; //(;
    float friction = 0;
    float particleSize = 0.1f;

    float despawnVel = 5f;

    boolean posIsSet = false;
    Vector2 position;
    Vector2 radius;

    boolean explode = false;
    boolean exploding = false;
    boolean exploded = false;

    public Explosion_Component(GameObject parent) {
        super(parent);

    }

    //TODO: Set Group Index for not colliding with each other
    public void spawnExplosionParticles(Vector2 position, Vector2 radius, int count){
        float deltaDeg = 360 / count;

        for(int i = 0; i < 360; i += deltaDeg){
            radius.setAngle(i);

            GameObject gameObject = new GameObject(GameObjectTypes.EXPLOSION_PARTICLE);
            FixtureDef fDef = ST_game.b2Body_creator.createFixtureDef(
                    ST_game.b2Body_creator.createCircleShape(particleSize),density, false, friction);


            B2Body_Component b2Body_component = new B2Body_Component(gameObject,
                    ST_game.b2Body_creator.createBodyDef(radius.x + position.x,radius.y + position.y,
                            BodyDef.BodyType.DynamicBody),fDef,
                    new BodyUserData(null, gameObject), particlesMass);

            gameObject.addComponent(b2Body_component);
            b2Body_component.b2body.setLinearDamping(linearDamping);
            b2Body_component.b2body.setGravityScale(gravityScale); //Ignoring Gravity

            fDef.restitution = restitution;
            fDef.filter.groupIndex = groupIndex;

            particles.add(gameObject);
        }

        setPositionOfExplosion(radius, position);
    }

    public void boom(){
        float deltaDeg = 360 / particles.size();
        int i = 0;

        for (GameObject gameObject:particles) {
            B2Body_Component b2Body_component;

            if(null != (b2Body_component = (B2Body_Component) (gameObject.getComponentsOfType(B2Body_Component.class.getName()).get(0)))) {
                float angleDeg = deltaDeg * i;
                Vector2 impulse = new Vector2(explosionForce * particlesMass, 0).setAngle(angleDeg);

                b2Body_component.b2body.applyLinearImpulse(impulse, b2Body_component.b2body.getWorldCenter(), true);
            }
            i++;
        }
    }

    public void setPositionOfExplosion_SAVE(Vector2 radius, Vector2 position){
        this.radius = radius;
        this.position = position;
        posIsSet = true;
    }

    public void setExplode(boolean explode){
        this.explode = explode;
    }

    //TODO: Make Use of this in Pool of Explosions -> Saves Garbage Collector Time
    private void setPositionOfExplosion(Vector2 radius, Vector2 position){
        if(particles.size() == 0) {
            System.out.println(this.toString() + "particle size was 0 | Maybe you forgot to spawn them");
            return;
        }

        float deltaDeg = 360 / particles.size();
        int i = 0;

        for (GameObject gameObj:particles) {
            B2Body_Component bodyComp;
            if(null != ( bodyComp = (B2Body_Component)gameObj.getComponentsOfType(B2Body_Component.class.getName()).get(0))) {
                bodyComp.b2body.setTransform(position.add(radius.setAngle(i * deltaDeg)), deltaDeg * i);
            }

            i++;
        }
    }

    public int getParticlesCount(){
        return this.particles.size();
    }

    @Override
    public void update(float dt) {
        if(posIsSet){
            setPositionOfExplosion(radius,position);
            posIsSet = false;
        }

        if(explode) {
            boom();
            explode = false;
            exploding = true;
        }

        if(exploding){
            int numOfDestroyedParticles = 0;
            for (GameObject gameObj:particles) {
                B2Body_Component b2Body_component;
                if(null != (b2Body_component = (B2Body_Component) gameObj.getComponentsOfType(B2Body_Component.class.getName()).get(0))){
                    float speed = b2Body_component.b2body.getLinearVelocity().len();

                    if(speed < despawnVel){
                        gameObj.dispose();
                    }
                }
                else {
                    numOfDestroyedParticles++;
                }
            }

            if(numOfDestroyedParticles == particles.size()){
                exploding = false;
                exploded = true;
            }
        }
    }

    public boolean getExploded(){
        return exploded;
    }

    public boolean getExploding(){
        return exploding;
    }
}

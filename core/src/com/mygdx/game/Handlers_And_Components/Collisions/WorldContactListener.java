package com.mygdx.game.Handlers_And_Components.Collisions;

import com.badlogic.gdx.physics.box2d.Contact;
import com.badlogic.gdx.physics.box2d.ContactImpulse;
import com.badlogic.gdx.physics.box2d.ContactListener;
import com.badlogic.gdx.physics.box2d.Fixture;
import com.badlogic.gdx.physics.box2d.Manifold;
import com.mygdx.game.Handlers_And_Components.GameObjects.GameObject;
import com.mygdx.game.Handlers_And_Components.GameObjects.GameObjectTypes;
import com.mygdx.game.Handlers_And_Components.GameObjects.GameObjects_Handler;

import java.util.AbstractMap;

public class WorldContactListener implements ContactListener {
    @Override
    public void beginContact(Contact contact) {
        Fixture fixA = contact.getFixtureA();
        Fixture fixB = contact.getFixtureB();

        Object userData_A = fixA.getUserData();
        Object userData_B = fixB.getUserData();

        ICollisionCallback collisionCallback_A = null;
        ICollisionCallback collisionCallback_B = null;

        GameObject gameObject_A = null;
        GameObject gameObject_B = null;

        /*if (fixA.getFilterData().maskBits == 1 || fixB.getFilterData().maskBits == 1) {
            return; //TODO: Right Collision-Filtering
        }*/
        //TODO: Potential Errors: UserData not on Fixture but on Body..
        if (userData_A != null && userData_A instanceof BodyUserData) {
            BodyUserData bodyUserData = (BodyUserData) userData_A;

            gameObject_A = bodyUserData.gameObject;
            collisionCallback_A = bodyUserData.collisionCallback;
        }
        if (userData_B != null && userData_B instanceof BodyUserData) {
            BodyUserData bodyUserData = (BodyUserData) userData_B;

            gameObject_B = bodyUserData.gameObject;
            collisionCallback_B = bodyUserData.collisionCallback;
        }

        if(collisionCallback_A instanceof GameObjects_Handler && gameObject_B != null){
            collisionCallback_A.beginCollisionCallback(null, gameObject_B); //Should not affect Collisions but should be messaged
        }
        else if(collisionCallback_B instanceof GameObjects_Handler && gameObject_B != null){
            collisionCallback_B.beginCollisionCallback(null, gameObject_A); //Should not affect Collisions but should be messaged
        }
        else {
            if(collisionCallback_A != null){
                collisionCallback_A.beginCollisionCallback(gameObject_A,gameObject_B);
            }
            else if (collisionCallback_B != null){
                collisionCallback_B.beginCollisionCallback(gameObject_A,gameObject_B);
            }
        }

        //TODO: Right Collision-Filtering
        /*if (collisionCallback_A != null && fixB.getFilterData().maskBits != 1) {
            collisionCallback_A.beginCollisionCallback(gameObject_A, gameObject_B);
        }
        if (collisionCallback_B != null && fixA.getFilterData().maskBits != 1) {
            collisionCallback_B.beginCollisionCallback(gameObject_A, gameObject_B);
        }*/

        /*if (userData_A != null && userData_A instanceof ICollisionCallback) {
            ICollisionCallback callback = (ICollisionCallback) userData_A;
            callback.beginCollisionCallback();
        }

        if (userData_B != null && userData_B instanceof ICollisionCallback) {
            ICollisionCallback callback = (ICollisionCallback) userData_B;
            callback.beginCollisionCallback();
        }*/
    }

    @Override
    public void endContact(Contact contact) {
        Fixture fixA = contact.getFixtureA();
        Fixture fixB = contact.getFixtureB();

        Object userData_A = fixA.getUserData();
        Object userData_B = fixB.getUserData();

        ICollisionCallback collisionCallback_A = null;
        ICollisionCallback collisionCallback_B = null;

        GameObject gameObject_A = null;
        GameObject gameObject_B = null;

        if (userData_A != null && userData_A instanceof BodyUserData) {
            BodyUserData bodyUserData = (BodyUserData) userData_A;

            collisionCallback_A = bodyUserData.collisionCallback;
            gameObject_A = bodyUserData.gameObject;
        }
        if (userData_B != null && userData_B instanceof BodyUserData) {
            BodyUserData bodyUserData = (BodyUserData) userData_B;

            collisionCallback_B = bodyUserData.collisionCallback;
            gameObject_B = bodyUserData.gameObject;
        }


        if(collisionCallback_A instanceof GameObjects_Handler && gameObject_B != null){
            collisionCallback_A.endCollisionCallback(null, gameObject_B);
        }
        else if(collisionCallback_B instanceof GameObjects_Handler && gameObject_B != null){
            collisionCallback_B.endCollisionCallback(null, gameObject_A);
        }
        else {
            if(collisionCallback_A != null){
                collisionCallback_A.endCollisionCallback(gameObject_A,gameObject_B);
            }
            else if (collisionCallback_B != null){
                collisionCallback_B.endCollisionCallback(gameObject_A,gameObject_B);
            }
        }
    }

    @Override
    public void preSolve(Contact contact, Manifold oldManifold) {

    }

    @Override
    public void postSolve(Contact contact, ContactImpulse impulse) {

    }
}

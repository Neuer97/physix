package com.mygdx.game.Handlers_And_Components.Collisions.FootSensor;

import com.mygdx.game.Handlers_And_Components.Collisions.ICollisionCallback;
import com.mygdx.game.Handlers_And_Components.GameObjects.GameObject;
import com.mygdx.game.Handlers_And_Components.GameObjects.GameObjectTypes;
import com.mygdx.game.Handlers_And_Components.Movable.Jumping_Component;
import com.mygdx.game.Objects.Person;

public class FootSensorCallback implements ICollisionCallback {
    Jumping_Component jumpingComponent;
    int touchCount = 0;

    public FootSensorCallback(GameObject gameObject, Jumping_Component jumping_component){
        this.jumpingComponent = jumping_component;
    }

    @Override
    public void beginCollisionCallback(GameObject gameObject_A, GameObject gameObject_B) {
        //TODO: Implement ENUM Types
        GameObject gameObject = null;
        if(gameObject_A != null && gameObject_A.getType() != GameObjectTypes.PERSON){
            gameObject = gameObject_A; //TODO: Potential Error Source -> Later Both could be != null and it means something useful
        }
        else if(gameObject_B != null && gameObject_B.getType() != GameObjectTypes.PERSON){
            gameObject = gameObject_B;
        }


        if(gameObject != null){
            if(gameObject.getType() == GameObjectTypes.PROJECTILE ||
                    gameObject.getType() == GameObjectTypes.GUN)
                return;
        }
        touchCount++;

        if(touchCount > 0){
            jumpingComponent.setJumpingEnabled(true);
        }
    }

    @Override
    public void endCollisionCallback(GameObject gameObject_A, GameObject gameObject_B) {
        GameObject gameObject = null;
        if(gameObject_A != null && gameObject_A.getType() != GameObjectTypes.PERSON){
            gameObject = gameObject_A; //TODO: Potential Error Source -> Later Both could be != null and it means something useful
        }
        else if(gameObject_B != null && gameObject_B.getType() != GameObjectTypes.PERSON){
            gameObject = gameObject_B;
        }


        if(gameObject != null){
            if(gameObject.getType() == GameObjectTypes.PROJECTILE ||
                    gameObject.getType() == GameObjectTypes.GUN)
                return;
        }


        touchCount--;

        if(touchCount <= 0){
            jumpingComponent.setJumpingEnabled(false);

            if(touchCount < 0){
                touchCount = 0;
            }
        }
    }
}

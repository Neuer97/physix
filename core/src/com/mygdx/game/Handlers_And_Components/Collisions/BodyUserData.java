package com.mygdx.game.Handlers_And_Components.Collisions;

import com.mygdx.game.Handlers_And_Components.GameObjects.GameObject;

public class BodyUserData {
    ICollisionCallback collisionCallback;
    GameObject gameObject;

    public  BodyUserData(ICollisionCallback collisionCallback, GameObject gameObject){
        this.collisionCallback = collisionCallback;
        this.gameObject = gameObject;
    }
}

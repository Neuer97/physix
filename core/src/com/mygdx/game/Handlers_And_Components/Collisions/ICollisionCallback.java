package com.mygdx.game.Handlers_And_Components.Collisions;

import com.mygdx.game.Handlers_And_Components.GameObjects.GameObject;

public interface ICollisionCallback {
    void beginCollisionCallback(GameObject gameObject_A, GameObject gameObject_B);
    void endCollisionCallback(GameObject gameObject_A, GameObject gameObject_B);
}

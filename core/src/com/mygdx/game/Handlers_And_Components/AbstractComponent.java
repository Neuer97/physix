package com.mygdx.game.Handlers_And_Components;

import com.mygdx.game.Handlers_And_Components.GameObjects.GameObject;

public abstract class AbstractComponent {
    GameObject parent;

    public AbstractComponent(GameObject parent){
        this.parent = parent;
        parent.addComponent(this);
    }

    public GameObject getParent() {
        return parent;
    }
}

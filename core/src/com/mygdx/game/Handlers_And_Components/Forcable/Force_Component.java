package com.mygdx.game.Handlers_And_Components.Forcable;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.mygdx.game.Handlers_And_Components.AbstractComponent;
import com.mygdx.game.Handlers_And_Components.GameObjects.GameObject;
import com.mygdx.game.Handlers_And_Components.IUpdatable;

//Gathers all Forces and the applies them
public class Force_Component extends AbstractComponent implements IUpdatable {
    Vector2 impulse = new Vector2();
    Vector2 force = new Vector2();

    public Body b2body;

    public Force_Component(GameObject gameObject, Body b2body){
        super(gameObject);
        this.b2body = b2body;
    }

    public void applyImpulseToCenter(float xImpulse, float yImpulse){
        this.impulse.add(xImpulse,yImpulse);
    }

    public void applyForceToCenter(float xForce, float yForce){
        this.force.add(xForce,yForce);
    }

    @Override
    public void update(float dt) {
       b2body.applyLinearImpulse(impulse, b2body.getWorldCenter(), true);
       impulse.set(0,0); //Reset

       b2body.applyForceToCenter(force,true);
       force.set(0,0); //Reset
    }
}

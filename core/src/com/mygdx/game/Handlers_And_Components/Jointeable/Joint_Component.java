package com.mygdx.game.Handlers_And_Components.Jointeable;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.Joint;
import com.badlogic.gdx.physics.box2d.JointDef;
import com.badlogic.gdx.physics.box2d.joints.RevoluteJoint;
import com.mygdx.game.Handlers_And_Components.AbstractComponent;
import com.mygdx.game.Handlers_And_Components.GameObjects.GameObject;
import com.mygdx.game.States.ST_game;

public class Joint_Component extends AbstractComponent {
    Joint joint = null;
    Vector2 localAnchor;

    Body thisBody;
    Joint_Component otherJointComponent;

    float motorSpeed = 0; //TODO: Put this in seperate RevololuteJoint something

    public Joint_Component(GameObject parent,Body body, float jointPosX, float jointPosY) {
        super(parent);

        this.localAnchor = new Vector2(jointPosX, jointPosY);
        this.thisBody = body;
    }

    public void createRevoluteJointJoint(Joint_Component otherJointComponent, boolean collideConnected, boolean enableMotor,float motorSpeed, float motorTorque){
        this.joint = ST_game.b2Body_creator.createRevoluteJoint(this.thisBody, otherJointComponent.getBody(), collideConnected,
                this.localAnchor.x, this.localAnchor.y,
                otherJointComponent.localAnchor.x, otherJointComponent.localAnchor.y, enableMotor, motorSpeed, motorTorque);

        this.motorSpeed = motorSpeed;
        otherJointComponent.joint = this.joint;

        this.otherJointComponent = otherJointComponent; //Reference Both of them and vice versa
        otherJointComponent.otherJointComponent = this; //Reference Both of them and vice versa
    }

    public void createWheelJoint(Joint_Component otherJointComponent, boolean collideConnected, boolean enableMotor,float motorSpeed, float motorTorque,
                                 float suspensionDampingRatio,float suspensionFrequencyHZ,
                                 Body bodyA, Body bodyB,
                                 Vector2 localAxisA){

        this.joint = ST_game.b2Body_creator.createWheelJoint(suspensionDampingRatio, suspensionFrequencyHZ, enableMotor, motorSpeed, motorTorque,
                bodyA, bodyB,
                this.localAnchor.x, this.localAnchor.y,
                otherJointComponent.localAnchor.x, otherJointComponent.localAnchor.y,
                localAxisA, collideConnected);

        this.otherJointComponent = otherJointComponent;
        otherJointComponent.otherJointComponent = this;
    }

    public void createWeldJoint(Joint_Component otherJointComponent, boolean collideConnected){
        this.joint = ST_game.b2Body_creator.createWeldJoint(thisBody, otherJointComponent.getBody(),
                this.localAnchor.x,this.localAnchor.y,
                otherJointComponent.localAnchor.x, otherJointComponent.localAnchor.y,
                collideConnected);
    }

    public void destroyJoint(){
        ST_game.b2Body_creator.destroyJoint(this.joint);
    }
    public boolean isSnapedOn(){
        return joint != null;
    }

    public void rotateTo(float angle){
        if(this.joint.getType() == JointDef.JointType.RevoluteJoint) {
            RevoluteJoint revoluteJoint = ((RevoluteJoint)this.joint);
            float jointAngle = (float)Math.toDegrees(revoluteJoint.getJointAngle());
            float deltaAngle = angle - jointAngle;

            //TODO: Not a very good Way to solve this
            //Calculates the fastest way by shortening the Angles, bc without rotation looks awkward
            while(deltaAngle > 180){
                deltaAngle = deltaAngle - 360;
            }
            while(deltaAngle < -180){
                deltaAngle = deltaAngle + 360;
            }

            revoluteJoint.setMotorSpeed(deltaAngle * motorSpeed);
            revoluteJoint.enableMotor(true);
        }
    }

    public void setMotorEnable(boolean motorEnabled){
        if(this.joint != null && this.joint.getType() == JointDef.JointType.RevoluteJoint){
            ((RevoluteJoint)this.joint).enableMotor(motorEnabled);
        }
    }
    public Body getBody() {
        return thisBody;
    }

    public Joint_Component getOtherJointComponent() {
        return otherJointComponent;
    }

    public float getAngleDeg(){
        return (float) Math.toDegrees(((RevoluteJoint)joint).getJointAngle()); //TODO: Not Castable Exception!
    }
}

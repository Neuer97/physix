package com.mygdx.game.Handlers_And_Components.Dragable;

import com.badlogic.gdx.math.Vector2;
import com.mygdx.game.Handlers_And_Components.AbstractComponent;
import com.mygdx.game.Handlers_And_Components.B2Body.B2Body_Component;
import com.mygdx.game.Handlers_And_Components.GameObjects.GameObject;
import com.mygdx.game.Handlers_And_Components.IUpdatable;

public class Drag_Component extends AbstractComponent implements IUpdatable {
    B2Body_Component b2Body_component;
    Vector2 pointToApply;

    float dragConstant;
    float mass;
    float angularDamping;

    public Drag_Component(GameObject parent, B2Body_Component b2Body_component, Vector2 pointToApply, float dragConstant, float mass, float angularDamping) {
        super(parent);

        this.b2Body_component = b2Body_component;
        this.pointToApply = pointToApply;
        this.dragConstant = dragConstant;
        this.mass = mass;
        this.angularDamping = angularDamping;

        b2Body_component.b2body.setAngularDamping(angularDamping);
    }


    @Override
    public void update(float dt) {
        Vector2 pointingDir = b2Body_component.b2body.getWorldVector(new Vector2(pointToApply.x, pointToApply.y));
        Vector2 flightDir = b2Body_component.b2body.getLinearVelocity();
        float speed = flightDir.nor().len(); //?

        float dot = flightDir.dot(pointingDir);
        float dragForce = (1 - Math.abs(dot)) * speed * speed * dragConstant * mass;

        Vector2 tail = b2Body_component.b2body.getWorldPoint(new Vector2(-pointToApply.x, pointToApply.y));

        b2Body_component.b2body.applyForce(flightDir.scl(-dragForce), tail, true);
    }
}

package com.mygdx.game.Handlers_And_Components;


public interface IUsable {
    void use();
}

package com.mygdx.game.Handlers_And_Components.InputHandler;

public abstract class InputHandler {
    InputScheme inputScheme = new InputScheme();
    InputScheme risingInputScheme = new InputScheme();

    public InputHandler(){
    }

    public InputScheme getInputScheme(Object params){
        updateInputScheme(params);
        return inputScheme;
    }

    public InputScheme getInputSchemeWRising(Object params){
        InputScheme inputSchemeOld = new InputScheme();
        updateInputScheme(params);

        if(hasRisingEdge(inputSchemeOld.isUsing, inputScheme.isUsing)){
            risingInputScheme.isUsing = true; //TODO: Update Inputs via Hashmap maybe
        }

        return inputScheme;
    }

    public boolean hasRisingEdge(boolean oldVal, boolean newVal){
        if(!oldVal && newVal){
            return true;
        }
        return false;
    }

    public abstract void updateInputScheme(Object params);
}

package com.mygdx.game.Handlers_And_Components.InputHandler;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.Input;
import com.badlogic.gdx.InputProcessor;
import com.badlogic.gdx.graphics.OrthographicCamera;
import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.math.Vector3;

public class MouseAndKeysInputHandler extends InputHandler implements InputProcessor {
    private static final int keyCodeUp = Input.Keys.W;
    private static final int keyCodeLeft = Input.Keys.A;
    private static final int keyCodeRight = Input.Keys.D;
    private static final int keyCodeDown = Input.Keys.S;
    private static final int keyCodeUse = Input.Keys.E;

    Vector2 aimingReference;

    float jumpingInputHeight = 1f;
    float movingInputHeight = 1f;

    OrthographicCamera camera;

    public MouseAndKeysInputHandler(OrthographicCamera camera) {
        Gdx.input.setInputProcessor(this);

        this.camera = camera;
    }

    @Override
    public void updateInputScheme(Object params) {
        if (params instanceof Vector2) {
            aimingReference = (Vector2) params;
        } else {
            System.out.println(this.getClass().getName() + ": Update Input Scheme -> Params were null where they should'nt be");
        }
    }

    @Override
    public boolean keyDown(int keycode) {
        return setKeyCode(true, keycode);
    }

    @Override
    public boolean keyUp(int keycode) {
        return setKeyCode(false, keycode);
    }

    private boolean setKeyCode(boolean state, int keycode) {
        switch (keycode) {

            case keyCodeUp:
                this.inputScheme.isJumping = state;
                this.inputScheme.moving.y = jumpingInputHeight;
                break;
            case keyCodeDown:

                inputScheme.moving.y = -jumpingInputHeight;
                break;


            case keyCodeRight:
                this.inputScheme.isMoving = state;
                this.inputScheme.moving.x = +movingInputHeight;
                break;
            case keyCodeLeft:
                this.inputScheme.isMoving = state;
                this.inputScheme.moving.x = -movingInputHeight;
                break;

            case keyCodeUse:
                this.inputScheme.isUsing = state;
                break;
        }

        return true;
    }

    @Override
    public boolean keyTyped(char character) {
        return false;
    }

    @Override
    public boolean touchDown(int screenX, int screenY, int pointer, int button) {
         return setMouseCode(true,button);
    }

    @Override
    public boolean touchUp(int screenX, int screenY, int pointer, int button) {
        inputScheme.isAiming = false;
        return setMouseCode(false,button);
    }

    private boolean setMouseCode(boolean state, int button){
        switch (button){
            case
                    Input.Buttons.LEFT: inputScheme.setShootingPrimary(state);
            break;
        }

        return true;
    }

    @Override
    public boolean touchDragged(int screenX, int screenY, int pointer) {
        mouseMoved(screenX, screenY);
        return true;
    }

    @Override
    public boolean mouseMoved(int screenX, int screenY) {
        if (aimingReference == null) {
            return true;
        }


        Vector3 mouseWorldPos_tmp = camera.unproject(new Vector3((float) screenX, (float) screenY, 0f)); //Get World Coordinates;
        Vector2 mouseWorldPos = new Vector2(mouseWorldPos_tmp.x, mouseWorldPos_tmp.y);

        Vector2 angleVector = mouseWorldPos.sub(aimingReference);

        float angleDeg = angleVector.angle();
        Vector2 aim = new Vector2(1, 0);
        aim.setAngle(angleDeg);

        inputScheme.aiming = aim;
        inputScheme.isAiming = true;
        return true;
    }

    @Override
    public boolean scrolled(int amount) {
        return false;
    }
}

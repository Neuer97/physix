package com.mygdx.game.Handlers_And_Components.InputHandler;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.scenes.scene2d.ui.Touchpad;

public class TouchInputHandler extends InputHandler {
    float shootingThreshold = 0.6f;
    float jumpingThreshold = 0.6f;
    float usingThreshold = 0.6f; //Left-Touchpad Down TODO: Add a Button
    float movingThreshold = 0f;
    float amingThreshold = 0f;


    Touchpad touchpadL;
    Touchpad touchpadR;

    public TouchInputHandler(Touchpad touchpadL, Touchpad touchpadR) {
        this.touchpadL = touchpadL;
        this.touchpadR = touchpadR;
    }

    @Override
    public void updateInputScheme(Object params) {
        boolean isShootingPrimary = false;
        boolean isJumping = false;
        boolean isMoving = false;
        boolean isAming = false;

        boolean isUsing = false;
        boolean isUsing_Rising = true;

        Vector2 moving = new Vector2(touchpadL.getKnobPercentX(), touchpadL.getKnobPercentY());
        Vector2 aiming = new Vector2(touchpadR.getKnobPercentX(), touchpadR.getKnobPercentY());


        if(inputScheme.isUsing){
            isUsing_Rising = false;
        }


        if((Math.abs(moving.len()) > jumpingThreshold)){
            isJumping = true;
        }
        if(Math.abs(moving.y) > usingThreshold){
            isUsing = true;
        }

        if(Math.abs(moving.x) > movingThreshold){
            isMoving = true;
        }

        if(Math.abs(aiming.len()) > amingThreshold){
            isAming = true;
        }

        if(Math.abs((new Vector2(touchpadR.getKnobPercentX(), touchpadR.getKnobPercentY())).len()) > shootingThreshold){
            isShootingPrimary = true;
        }

        //TODO: Own Rising,Falling Logic
        if(isUsing == true && isUsing_Rising == true){
            isUsing_Rising = true;
        }
        else {
            isUsing_Rising = false;
        }
        inputScheme.updateInputScheme(touchpadL.getKnobPercentX(), touchpadL.getKnobY(),
                touchpadR.getKnobPercentX(), touchpadR.getKnobPercentY(),
                isShootingPrimary,
                isJumping, isMoving, isAming,isUsing);
    }
}

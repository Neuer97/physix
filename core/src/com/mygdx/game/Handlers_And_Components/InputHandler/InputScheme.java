package com.mygdx.game.Handlers_And_Components.InputHandler;

import com.badlogic.gdx.math.Vector2;

public class InputScheme {
    boolean shootingPrimary;
    boolean isJumping = false;
    boolean isMoving = false;
    boolean isAiming = false;

    boolean isUsing = false;

    Vector2 aiming;
    Vector2 moving;

    public InputScheme(){
        aiming = new Vector2(0,0);
        moving = new Vector2(0,0);

        shootingPrimary = false;
    }

    public void updateInputScheme(InputScheme inputScheme){
        updateInputScheme(inputScheme.moving.x, inputScheme.moving.y,
                inputScheme.aiming.x,inputScheme.aiming.y,
                inputScheme.shootingPrimary,
                inputScheme.isJumping,inputScheme.isMoving,
                inputScheme.isAiming,inputScheme.isUsing);
    }

    public void updateInputScheme(float movingX,float movingY,
                                  float aimingX, float aimingY,
                                  boolean shootingPrimary,
                                  boolean jumping, boolean isMoving, boolean isAiming, boolean isUsing){
        setAiming(aimingX, aimingY);
        setMoving(movingX, movingY);
        setShootingPrimary(shootingPrimary);
        this.isJumping = jumping;
        this.isMoving = isMoving;
        this.isAiming = isAiming;
        this.isUsing = isUsing;
    }

    public boolean isShootingPrimary() {
        return shootingPrimary;
    }

    public void setShootingPrimary(boolean shootingPrimary) {
        this.shootingPrimary = shootingPrimary;
    }

    public Vector2 getAiming() {
        return aiming;
    }

    public void setAiming(float aimingX, float aimingY) {
        this.aiming.set(aimingX, aimingY);
    }

    public Vector2 getMoving() {
        return moving;
    }

    public void setMoving(float movingX, float movingY) {
        this.moving.set(movingX, movingY);
    }

    public boolean isJumping() {
        return isJumping;
    }

    public boolean isMoving(){
        return isMoving;
    }

    public boolean isAiming(){
        return isAiming;
    }

    public boolean isUsing(){
        return isUsing;
    }

    public boolean getIsUsingOnce(){
        boolean tmp = isUsing;
        this.isUsing = false;
        return tmp;
    }
}

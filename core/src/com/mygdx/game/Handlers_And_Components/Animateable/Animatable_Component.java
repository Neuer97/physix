package com.mygdx.game.Handlers_And_Components.Animateable;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.mygdx.game.Handlers_And_Components.AbstractComponent;
import com.mygdx.game.Handlers_And_Components.Drawable.Drawable_Component;
import com.mygdx.game.Handlers_And_Components.GameObjects.GameObject;
import com.mygdx.game.Handlers_And_Components.IUpdatable;

import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;

public abstract class Animatable_Component extends AbstractComponent implements IUpdatable {
    private HashMap<String, Animation> allAnimations;
    private List<Animation> currentAnimations;

    private float stateTime = 0;

    protected Drawable_Component drawableComp_toModify;


    public Animatable_Component(GameObject gameObject,Drawable_Component toModify, Animation animation) {
        super(gameObject);

        this.allAnimations = new HashMap<String, Animation>();
        this.currentAnimations = new LinkedList<Animation>();

        this.drawableComp_toModify = toModify;

        if (animation != null)
            this.allAnimations.put(animation.name, animation);
    }

    public Animatable_Component(GameObject gameObject,Drawable_Component toModify,List<Animation> allAnimations) {
        super(gameObject);

        this.allAnimations = new HashMap<String, Animation>();
        this.currentAnimations = new LinkedList<Animation>();

        this.drawableComp_toModify = toModify;

        for (int i = 0; i < allAnimations.size(); i++) {
            this.allAnimations.put(allAnimations.get(i).name, allAnimations.get(i));
        }

    }

    public void addAllAmition(Animation animation){
        allAnimations.put(animation.name,animation);
    }

    public void removeAllAnimatio(Animation animation){
        allAnimations.remove(animation);
    }

    //ADDING ANIMATIONS______________________________
    public void addCurrentAnimation(String name) {
        addCurrentAnimation(allAnimations.get(name));
    }


    private void addCurrentAnimation(Animation animation) {
        if(!currentAnimations.contains(animation)) {
            currentAnimations.add(animation);
            Collections.sort(currentAnimations, animComparator);
        }
    }
    //________________________________________________

    //REMOVING ANIMATIONS
    public void removeCurrentAnimation(String name) {
        for (Animation animation : currentAnimations) {
            if (animation.name.equals(name)) {
                removeCurrentAnimation(animation);
            }
        }
    }

    private void removeCurrentAnimation(Animation animation) {
        currentAnimations.remove(animation);

        Collections.sort(currentAnimations, animComparator);
    }
    //_________________________________________________


    public TextureRegion getFrame(float dt) {
        if (currentAnimations.size() == 0) {
            return null;
        }
        stateTime += dt;

        return currentAnimations.get(0).getKeyFrame(stateTime);
    }

    //Loads Animations in currAnimations accordingly on what's happening -> e.g. checks Velocity of B2Body and adds "Running"-Animation
    public abstract boolean animatableSpecific_update();

    public void update(float dt){
        if(animatableSpecific_update()) {
            drawableComp_toModify.setAnimatedTexture(getFrame(dt));
        }
        else {
            drawableComp_toModify.resetAnimatedTexture();
        }
    }

    //Comparator___________________________________________________________________________
    private Comparator<Animation> animComparator = new Comparator<Animation>() {
        @Override
        public int compare(Animation animation, Animation t1) {
            return animation.compareTo(t1);
        }
    };

    public int getLenghtOfCurrAnimations(){
        return this.currentAnimations.size();
    }
}

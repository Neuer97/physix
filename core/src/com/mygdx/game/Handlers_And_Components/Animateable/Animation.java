package com.mygdx.game.Handlers_And_Components.Animateable;

import com.badlogic.gdx.graphics.g2d.TextureRegion;
import com.badlogic.gdx.utils.Array;

public class Animation extends com.badlogic.gdx.graphics.g2d.Animation<TextureRegion> implements Comparable {
    String name;
    int prio; //The higher it is, the more likely the Animation is going to be Player (dead > jumping > running)


    public Animation(String name, int prio, float frameDuration,Array<TextureRegion> frames){
        super(frameDuration, frames);

        this.name = name;
        this.prio = prio;
    }

    public Animation(String name, int prio,  float frameDuration, PlayMode playMode, Array<TextureRegion> frames){
        super(frameDuration, frames, playMode);

        this.name = name;
        this.prio = prio;
    }

    @Override
    public int compareTo(Object o) {
        return compareTo((Animation)o);
    }

    public int compareTo(Animation animation) {
        return animation.prio - this.prio;
    }
}

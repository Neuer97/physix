package com.mygdx.game.Handlers_And_Components;

public interface IUpdatable {
    void update(float dt);
}

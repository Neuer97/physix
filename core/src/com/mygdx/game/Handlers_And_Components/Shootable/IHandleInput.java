package com.mygdx.game.Handlers_And_Components.Shootable;

import com.mygdx.game.Handlers_And_Components.InputHandler.InputScheme;

public interface IHandleInput {
    void handleInput(InputScheme inputScheme, float dt);
}

package com.mygdx.game.Handlers_And_Components.Shootable;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.utils.Timer;
import com.mygdx.game.Handlers_And_Components.GameObjects.GameObjectTypes;
import com.mygdx.game.Handlers_And_Components.AbstractComponent;
import com.mygdx.game.Handlers_And_Components.B2Body.B2Body_Component;
import com.mygdx.game.Handlers_And_Components.Collisions.BodyUserData;
import com.mygdx.game.Handlers_And_Components.GameObjects.GameObject;
import com.mygdx.game.Handlers_And_Components.IUpdatable;
import com.mygdx.game.Objects.Gun;
import com.mygdx.game.Objects.Projectile;
import com.mygdx.game.States.ST_game;

import java.util.LinkedList;
import java.util.List;

//TODO: IUpdatable not right here! When Shootable_Component gets out of sight, it should behave same
public class Shootable_Component extends AbstractComponent implements IUpdatable {
    B2Body_Component b2Body_component;
    List<Projectile> projectiles;

    int clipSize;
    int currentClipSize;

    float RPM;
    long lastTimeShot = 0;

    float reloadTime;
    boolean reloading = false;

    float shootingForce;
    float bulletWidth;
    float bulletHeight;
    float bulletMass = 5f;
    float dragConstant = 0.1f;

    public Shootable_Component(GameObject gameObject, B2Body_Component b2Body_component,
                               int clipSize, float reloadTime, float RPM, float shootingForce, float bulletWidth, float bulletHeight){
        super(gameObject);

        this.clipSize = clipSize;
        this.currentClipSize = clipSize;

        this.reloadTime = reloadTime;
        this.RPM = RPM;
        this.shootingForce = shootingForce;
        this.bulletWidth = bulletWidth;
        this.bulletHeight = bulletHeight;
        projectiles = new LinkedList<Projectile>();

        this.b2Body_component = b2Body_component;
    }

    public void shoot(Vector2 barrelEnd, Vector2 tilt) {
        if(!canShoot()){
            return;
        }
        if(currentClipSize < 1){
            reload();
            return;
        }

        projectiles.add(new Projectile(bulletWidth,bulletHeight,bulletMass,dragConstant, barrelEnd,tilt,shootingForce * bulletMass,b2Body_component.b2body.getLinearVelocity()));


        lastTimeShot = System.currentTimeMillis();
        currentClipSize--;
    }

    public void reload(){
        //TODO: Verify

        if(!reloading) {
            reloading = true;


            Timer.schedule(new Timer.Task() {
                @Override
                public void run() {
                    currentClipSize = clipSize;
                    reloading = false;
                }
            }, reloadTime);
        }
    }

    public boolean canShoot(){
        float deltaMillis = (60 / RPM) * 1000/*ms*/;

        if(System.currentTimeMillis() - lastTimeShot > deltaMillis){
            return true;
        }
        else {
            return false;
        }
    }

    @Override
    public void update(float dt) {

        for (Projectile projectile:projectiles) {
            IUpdatable iUpdatable;


            if(null != (iUpdatable = projectile.getUpdatableComponents().get(0))){
                iUpdatable.update(dt);//Potential Error source: More than one IUpdatable
            }
        }
    }
}

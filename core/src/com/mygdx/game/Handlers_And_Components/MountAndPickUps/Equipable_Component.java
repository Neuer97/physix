package com.mygdx.game.Handlers_And_Components.MountAndPickUps;

import com.badlogic.gdx.physics.box2d.Body;
import com.mygdx.game.Handlers_And_Components.GameObjects.GameObject;
import com.mygdx.game.Handlers_And_Components.Jointeable.Joint_Component;

public class Equipable_Component extends Pickupable_Component {
    float maxMotorSpeed;
    float motorTourque;

    public Equipable_Component(GameObject parent, Body usableBody, float maxMotorSpeed, float motorTourque) {
        super(parent, usableBody);

        this.maxMotorSpeed = maxMotorSpeed;
        this.motorTourque = motorTourque;
    }

    public void equip(GameObject user){
        Joint_Component joint_componentUser = (Joint_Component) user.getComponentsOfType(Joint_Component.class.getName()).get(0); //TODO: Null-Pointer Exception
        Joint_Component joint_componentUsed = (Joint_Component) this.getParent().getComponentsOfType(Joint_Component.class.getName()).get(0); //TODO: Null-Pointer Exception

        joint_componentUser.createRevoluteJointJoint(joint_componentUsed, false,false, maxMotorSpeed, motorTourque);

        user.addComponent(this);

        isUsed = true;
    }

    public void unequip(GameObject userOld){
        Joint_Component joint_componentUser = (Joint_Component) userOld.getComponentsOfType(Joint_Component.class.getName()).get(0); //TODO: Null-Pointer Exception
        //Joint_Component joint_componentUsed = (Joint_Component) this.getParent().getComponentsOfType(Joint_Component.class.getName()).get(0); //TODO: Null-Pointer Exception

        joint_componentUser.destroyJoint();

        isUsed = false;
    }

}

package com.mygdx.game.Handlers_And_Components.MountAndPickUps;

import com.badlogic.gdx.math.Vector3;
import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.World;
import com.badlogic.gdx.utils.viewport.Viewport;
import com.mygdx.game.Handlers_And_Components.Collisions.BodyUserData;
import com.mygdx.game.Handlers_And_Components.Collisions.ICollisionCallback;
import com.mygdx.game.Handlers_And_Components.GameObjects.GameObject;
import com.mygdx.game.Handlers_And_Components.IUpdatable;
import com.mygdx.game.States.ST_game;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class PickUp_Handler implements ICollisionCallback, IUpdatable{
    private Viewport viewport;
    private float scalingFactor = 0.2f; //Initial Height && Width needs to be scaled that Textures are not popping in on the ScreenSides
    private Vector3 position;
    private Body sensorBody;


    List<GameObject> usableGameObjects = new LinkedList<GameObject>();
    List<Pickupable_Component> pickupable_components = new LinkedList<Pickupable_Component>();

    public PickUp_Handler(World world, Vector3 position, Viewport viewport){
        this.viewport = viewport;
        this.position = position;

        BodyDef bDef = ST_game.b2Body_creator.createBodyDef(position.x, position.y, BodyDef.BodyType.StaticBody);
        sensorBody = world.createBody(bDef);

        float width = viewport.getWorldWidth() * scalingFactor;
        float height = viewport.getWorldHeight() * scalingFactor;
        FixtureDef fdef = ST_game.b2Body_creator.createFixtureDef(
                (ST_game.b2Body_creator.createRectShape(width,height, 0,0, 0)),
                0, true, 0);
        fdef.filter.maskBits = 1; //No Collision //TODO: Right collision-Filtering

        sensorBody.createFixture(fdef).setUserData(new BodyUserData(this, null));
    }

    @Override
    public void update(float dt) {
        sensorBody.setTransform(position.x, position.y,0);
        //TODO: SetHeight && Width
    }

    @Override
    public void beginCollisionCallback(GameObject gameObject_A, GameObject gameObject_B) {
        GameObject gameObject = null;
        if(gameObject_A != null){
            gameObject = gameObject_A;
        }
        if(gameObject_B != null){
            gameObject = gameObject_B;
        }
        if(gameObject == null) {
            System.out.println("GameObjects_Handler: both GameObjects were null");
            return;
        }

        if(usableGameObjects.contains(gameObject) ||
                gameObject.getComponentsOfType(Pickupable_Component.class.getName()) == null ||
                gameObject.getComponentsOfType(Pickupable_Component.class.getName()).size() == 0){
            return;
        }
        usableGameObjects.add(gameObject);
        pickupable_components.addAll((List<Pickupable_Component>)((Object)gameObject.getComponentsOfType(Pickupable_Component.class.getName())));

        Collections.sort(pickupable_components);
    }

    @Override
    public void endCollisionCallback(GameObject gameObject_A, GameObject gameObject_B) {
        GameObject gameObject = null;
        if(gameObject_A != null){
            gameObject = gameObject_A;
        }
        if(gameObject_B != null){
            gameObject = gameObject_B;
        }
        if(gameObject == null) {
            System.out.println("GameObjects_Handler: both GameObjects were null");
            return;
        }
        if(!usableGameObjects.contains(gameObject)){
            return;
        }
        usableGameObjects.remove(gameObject);
        pickupable_components.removeAll((List<Pickupable_Component>)((Object)gameObject.getComponentsOfType(Pickupable_Component.class.getName())));

        Collections.sort(pickupable_components);
    }

    public List<Pickupable_Component> getPickups() {
        List<Pickupable_Component> pickubables = new LinkedList<Pickupable_Component>();
        for (Pickupable_Component pickupableComp:pickupable_components) {
            if(!pickupableComp.isUsed){
                pickubables.add(pickupableComp);
            }
        }
        return pickupable_components;
    }

    public Pickupable_Component getNearestPickup(){
        for (Pickupable_Component pickupableComp:pickupable_components) {
            if(!pickupableComp.isUsed){
                return pickupableComp;
            }
        }
        return null;
    }

    public boolean isUsableNear(){
        for (Pickupable_Component pickupableComp:pickupable_components) {
            if(!pickupableComp.isUsed){
                return true;
            }
        }
        return false;
    }
}

package com.mygdx.game.Handlers_And_Components.MountAndPickUps;

import com.badlogic.gdx.physics.box2d.Body;
import com.mygdx.game.Handlers_And_Components.B2Body.B2Body_Component;
import com.mygdx.game.Handlers_And_Components.GameObjects.GameObject;
import com.mygdx.game.States.ST_game;

public class Mountable_Component extends  Pickupable_Component{
    GameObject user;

    float dimountingTendencyX = -3f; //offsetX when dismounting from middle of Obj
    float dismountingTendencyY = 3; //offsetY when dismounting from middle of Obj

    public Mountable_Component(GameObject parent, Body usableBody) {
        super(parent, usableBody);
    }

    public void mount(GameObject user){
        ST_game.b2Body_creator.dump(user);
        this.user = user;

        isUsed = true;
    }

    public void dismount(){
        B2Body_Component b2Body_component = (B2Body_Component)this.getParent().getComponentsOfType(B2Body_Component.class.getName()).get(0);
        if(b2Body_component == null || b2Body_component.b2body == null){
            System.out.println(this.getClass().toString() + ": Throw Away -> Body Component of userOld was null");
            return;
        }

        ST_game.b2Body_creator.reSpawn(user,
                b2Body_component.b2body.getPosition().x - 3, b2Body_component.b2body.getPosition().y + 3,0);

        user = null;
        isUsed = false;
    }

    public GameObject getUser() {
        return user;
    }
}

package com.mygdx.game.Handlers_And_Components.MountAndPickUps;

import com.badlogic.gdx.math.Vector2;
import com.badlogic.gdx.physics.box2d.Body;
import com.mygdx.game.Handlers_And_Components.AbstractComponent;
import com.mygdx.game.Handlers_And_Components.GameObjects.GameObject;

//TODO: Make  Name Up-Pickable
public abstract class Pickupable_Component extends AbstractComponent implements Comparable<Pickupable_Component> {
    public Body usableBody;
    boolean isUsed;

    public Pickupable_Component(GameObject parent,Body usableBody) {
        super(parent);
        this.usableBody = usableBody;
    }


    public Vector2 getPosition(){
        return usableBody.getPosition();
    }

    public float getDistance(Pickupable_Component pickupable_Component){
        return (pickupable_Component.getPosition().sub(this.getPosition())).len();
    }

    @Override
    public int compareTo(Pickupable_Component pickupable_Component) {
        return (int)(this.getDistance((pickupable_Component))* 100);
    }
}

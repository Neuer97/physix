package com.mygdx.game.Handlers_And_Components.B2Body;

import com.badlogic.gdx.physics.box2d.Body;
import com.badlogic.gdx.physics.box2d.BodyDef;
import com.badlogic.gdx.physics.box2d.FixtureDef;
import com.badlogic.gdx.physics.box2d.MassData;
import com.badlogic.gdx.physics.box2d.World;
import com.mygdx.game.Handlers_And_Components.AbstractComponent;
import com.mygdx.game.Handlers_And_Components.Collisions.BodyUserData;
import com.mygdx.game.Handlers_And_Components.Collisions.ICollisionCallback;
import com.mygdx.game.Handlers_And_Components.GameObjects.GameObject;
import com.mygdx.game.Handlers_And_Components.IDisposable;
import com.mygdx.game.States.ST_game;

import java.util.AbstractMap;

public class B2Body_Component extends AbstractComponent implements IDisposable {
    public Body b2body;

    boolean disposed = false;

   public B2Body_Component(GameObject parent, BodyDef bDef, FixtureDef fDef, BodyUserData bodyUserData , float mass) {
        super(parent);

        b2body = ST_game.b2Body_creator.createBody(bDef);
        b2body.createFixture(fDef).setUserData(bodyUserData);


        b2body.getMassData().mass = mass;
    }

    @Override
    public void dispose() {
       if(!disposed) {
           ST_game.b2Body_creator.getWorld().destroyBody(b2body);
           disposed = true;
       }
    }
}
